// X64_Dbg_Addition.cpp : Defines the exported functions for the DLL application.
//
#include "X64_Dbg_Addition.h"
#include "pluginsdk/dbghelp/dbghelp.h"
#include "pluginmain.h"
#include "pluginsdk/BeaEngine/BeaEngine.h"
#include "pluginsdk/bridgemain.h"
#include "pluginsdk/BeaEngine/export.h"
#include "pluginsdk/jansson/jansson.h"
#include "pluginsdk/TitanEngine/TitanEngine.h"
#include "pluginsdk/XEDParse/XEDParse.h"
#include "resource.h"
#include "TitanHide.h"
#include "pluginsdk/_plugins.h"

//////////////////////////////////////////////////////////////////////////
//Define external Functions
FILLSYMBOLDATA FillSymbolData;
GOTODIALOG GoToDialog;
GOTODIALOGINNEWTHREAD GoToDialogInNewThread;
COPYSYSFIlE CopySysFile;
GOTOADDRRESS GoToAddrress;
MODULE_NAME_PATH_BASE Module_name_Path_Base;
SETCLIPBOARD SetClipboard;
API_CATEGORY_BP API_Category_BP;
GETBPOFLIST GetBPOFList;
//////////////////////////////////////////////////////////////////////////
//Define Functions
static void cbAdditionMenuEntry(CBTYPE cbType, void* callbackInfo);
static void CBLOADDLL(CBTYPE cbType, void* callbackInfo);
static bool GoTODialog(int argc, char* argv[]);
static bool TitanHide(int argc, char* argv[]);
static void CBCREATEPROCESS(CBTYPE cbType, void* callbackInfo);
static BOOL CALLBACK DlgMain(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
static ULONG GetTypeDword();
static void TitanHideCall(HIDE_COMMAND Command);
bool SysService();
bool startService();
static void SetCheckBoxVar(HWND hwndDlg);
static void SetCheckBoxValue(HWND hwndDlg);
static bool GoTo_Addrress(int argc, char* argv[]);
extern "C" DLL_EXPORT void BPList();
//////////////////////////////////////////////////////////////////////////
//variable define 
DWORD PID_Debuged_Process;
const char* SysPath;
const char* Load_Module_name;
const char * Module_Path;
bool CheckBox_PROCESSDEBUGFLAGS;
bool CheckBox_PROCESSDEBUGPORT;
bool CheckBox_PROCESSDEBUGOBJECTHANDLE;
bool CheckBox_DEBUGOBJECT;
bool CheckBox_SYSTEMDEBUGGERINFORMATION;
bool CheckBox_NTCLOSE;
bool CheckBox_THREADHIDEFROMDEBUGGER;
bool CheckBox_NTSETCONTEXTTHREAD;
//////////////////////////////////////////////////////////////////////////
#define serviceName "TitanHide"

static void cbInitDebug(CBTYPE cbType, void* callbackInfo)
{
    PLUG_CB_INITDEBUG* info=(PLUG_CB_INITDEBUG*)callbackInfo;	
	PID_Debuged_Process=((PROCESS_INFORMATION*)TitanGetProcessInformation())->dwProcessId; 
	//copy sys file
	SysPath=CopySysFile();
	SysService();
	startService();
	TitanHideCall(HidePid);
	//
	
	_plugin_logprintf("[x64_dbg_Addition] debugging of file %s started!\n", (const char*)info->szFileName);

	
}

static void cbStopDebug(CBTYPE cbType, void* callbackInfo)
{
    _plugin_logputs("[x64_dbg_Addition] debugging stopped!");
}

static void CBCREATEPROCESS(CBTYPE cbType, void* callbackInfo)
{
	PLUG_CB_CREATEPROCESS* info=(PLUG_CB_CREATEPROCESS*)callbackInfo;
	Module_name_Path_Base(info->modInfo->BaseOfImage,info->modInfo->ImageName,info->modInfo->ModuleName,true);
	//BPList();
	_plugin_logputs("[x64_dbg_Addition] debugging CREATEPROCESS!");
}

void BPList()
{
	//////////////////////////////////////////////////////////////////////////
	//Get BP list sample
	BPMAP ListOFBP;
	int x=DbgGetBpList(bp_normal,&ListOFBP);
	for(int i=0;i<ListOFBP.count;i++) 
	{
		//printf("%d addr %x",ListOFBP.bp[i]);	
		GetBPOFList(ListOFBP.bp[i]);
	}
	x=DbgGetBpList(bp_hardware,&ListOFBP);
	for(int ii=0;ii<ListOFBP.count;ii++) 
	{
		//printf("%d addr %x",ListOFBP.bp[i]);	
		GetBPOFList(ListOFBP.bp[ii]);
	}
	//BridgeFree(ListOFBP.bp);
	//return ListOFBP;
}


//////////////////////////////////////////////////////////////////////////

void AdditionInit(PLUG_INITSTRUCT* initStruct)
{
	_plugin_logprintf("[TEST] pluginHandle: %d\n", pluginHandle);
	_plugin_registercallback(pluginHandle, CB_INITDEBUG, cbInitDebug);
	_plugin_registercallback(pluginHandle, CB_STOPDEBUG, cbStopDebug);
	_plugin_registercallback(pluginHandle, CB_MENUENTRY, cbAdditionMenuEntry);
	_plugin_registercallback(pluginHandle, CB_LOADDLL, CBLOADDLL);
	_plugin_registercallback(pluginHandle, CB_CREATEPROCESS, CBCREATEPROCESS);
	//////////////////////////////////////////////////////////////////////////

	if(!_plugin_registercommand(pluginHandle, "GTD", GoTODialog, true))
		_plugin_logputs("[TEST] error registering the \"Addition plugin\" command!");

	if(!_plugin_registercommand(pluginHandle, "TitanHide", TitanHide, true)) //true=only when debugging
		_plugin_logputs("[TEST] error registering the \"Addition plugin\" command!");
	
	if(!_plugin_registercommand(pluginHandle, "GoTo_Addrress", GoTo_Addrress, true)) //true=only when debugging
		_plugin_logputs("[TEST] error registering the \"Addition plugin\" command!");

	//if(!_plugin_registercommand(pluginHandle, "FindCommand", FindCommand, true)) //true=only when debugging
	//	_plugin_logputs("[TEST] error registering the \"Addition plugin\" command!");

	//if(!_plugin_registercommand(pluginHandle, "EnableKeyReg", EnableKeyReg, true)) //true=only when debugging
	//	_plugin_logputs("[TEST] error registering the \"Addition plugin\" command!");

	//if(!_plugin_registercommand(pluginHandle, "FindIntermodulerCall", FindIntermodulerCall_x, true)) //true=only when debugging
	//	_plugin_logputs("[TEST] error registering the \"Addition plugin\" command!");

}

void AdditionStop()
{
	_plugin_unregistercallback(pluginHandle, CB_INITDEBUG);
	_plugin_unregistercallback(pluginHandle, CB_STOPDEBUG);
	_plugin_unregistercallback(pluginHandle, CB_MENUENTRY);
	_plugin_unregistercallback(pluginHandle, CB_CREATEPROCESS);
	_plugin_unregistercommand(pluginHandle, "GTD");
	_plugin_unregistercommand(pluginHandle, "TitanHide");
	_plugin_unregistercommand(pluginHandle, "GoTo_Addrress");
	/*_plugin_unregistercommand(pluginHandle, "FindCommand");
	_plugin_unregistercommand(pluginHandle, "EnableKeyReg");
	_plugin_unregistercommand(pluginHandle, "FindCommand");
	_plugin_unregistercommand(pluginHandle, "grs");*/
	_plugin_menuclear(hMenu);
}

void AdditionSetup()
{
	_plugin_menuaddentry(hMenu, GotoAPI, "&Go To APi");
	_plugin_menuaddentry(hMenu, TitanHide_Menu, "&TitanHide");
	_plugin_menuaddentry(hMenu, GoToAddrress_Menu, "&GoToAddrress");
	_plugin_menuaddentry(hMenu, API_Category_BP_Menu, "&API_Category_BP");
	_plugin_menuaddentry(hMenu, about, "&About...");	
	//////////////////////////////////////////////////////////////////////////
	// Add menu to disasm window
	_plugin_menuaddentry(hMenuDisasm,Disasm_Addition_GoToAPI,"&Go To APi");
	_plugin_menuaddentry(hMenuDisasm,Disasm_Addition_TitanHide,"&TitanHide");
	_plugin_menuaddentry(hMenuDisasm,Disasm_Addition_GoToAddrress,"&GoToAddrress");
	//////////////////////////////////////////////////////////////////////////
	//add menu and sub menu to Disasm window
	int Disasm_submenu_CopyAddres=_plugin_menuadd(hMenuDisasm,"&Copy Address");
	_plugin_menuaddentry(Disasm_submenu_CopyAddres,Disasm_Addition_CopyAddres_RVA,"&Relative Virtual Address");
	_plugin_menuaddentry(Disasm_submenu_CopyAddres,Disasm_Addition_CopyAddres_VA,"&Virtual Address");		
	_plugin_menuaddentry(hMenuDump,Dump_Addition_CopyAddres_RVA,"&Relative Virtual Address");
	_plugin_menuaddentry(hMenuDump,Dump_Addition_CopyAddres_VA,"&Virtual Address");
	//////////////////////////////////////////////////////////////////////////
	
	
	//////////////////////////////////////////////////////////////////////////
	// add menu and sub menu 
	//int mysubmenu=_plugin_menuadd(hMenu,"sub menu"); //this only adds a 'fake' new menu
	//_plugin_menuaddentry(mysubmenu, submenuentry, "sub menu entry"); //this is the new entry, it will be placed inside the submenu
	//45 will be the number of the entry presses
	//hMenu = also a submenu, but it is the submenu of the plugin
}

//////////////////////////////////////////////////////////////////////////
// Our Menu
static void cbAdditionMenuEntry(CBTYPE cbType, void* callbackInfo)
{
    PLUG_CB_MENUENTRY* info=(PLUG_CB_MENUENTRY*)callbackInfo;
	duint temp;
	duint x;
	int cc=1;
	SELECTIONDATA sel;
    switch(info->hEntry)
    {		
    case about:       
		MessageBoxA(GuiGetWindowHandle(),"Addition x64_dbg Plugins v0.4 ,Coded By Ahmadmansoor/exetools - Thanks for Mr.exodia","Info",0);
        break;
	case GotoAPI: //| Disasm_Addition_GoToAPI:
		DbgCmdExec("GTD");
        break;
	case Disasm_Addition_GoToAPI:
		DbgCmdExec("GTD");
		break;

	case TitanHide_Menu:
		DbgCmdExec("TitanHide");		
		break;
	case Disasm_Addition_TitanHide:
		DbgCmdExec("TitanHide");		
		break;

	case Disasm_Addition_GoToAddrress:
		DbgCmdExec("GoTo_Addrress");
		break;
	case GoToAddrress_Menu:
		DbgCmdExec("GoTo_Addrress");
		break;

	case Disasm_Addition_CopyAddres_RVA :
		GuiSelectionGet(GUI_DISASSEMBLY, &sel);
		x= DbgFunctions()->ModBaseFromAddr(sel.start);		
		SetClipboard(sel.start-x);		
		break;  
	case Disasm_Addition_CopyAddres_VA :
		GuiSelectionGet(GUI_DISASSEMBLY, &sel);
		//x= DbgFunctions()->ModBaseFromAddr(sel.start);
		SetClipboard(sel.start);
		break;
	case Dump_Addition_CopyAddres_RVA:
		GuiSelectionGet(GUI_DUMP, &sel);
		x= DbgFunctions()->ModBaseFromAddr(sel.start);		
		SetClipboard(sel.start-x);		
		break;  
	case Dump_Addition_CopyAddres_VA:
		GuiSelectionGet(GUI_DUMP, &sel);
		//x= DbgFunctions()->ModBaseFromAddr(sel.start);
		SetClipboard(sel.start);
		break;
	case API_Category_BP_Menu:

		API_Category_BP();
		break;
    }
	//case MENU_SELECTION:
	//    {
	//        SELECTIONDATA sel;
	//        char msg[256]="";
	//        GuiSelectionGet(GUI_DISASSEMBLY, &sel);
	//        sprintf(msg, "%p - %p", sel.start, sel.end);
	//        MessageBoxA(hwndDlg, msg, "Disassembly", MB_ICONINFORMATION);
	//        sel.start+=4; //expand selection
	//        GuiSelectionSet(GUI_DISASSEMBLY, &sel);

	//        GuiSelectionGet(GUI_DUMP, &sel);
	//        sprintf(msg, "%p - %p", sel.start, sel.end);
	//        MessageBoxA(hwndDlg, msg, "Dump", MB_ICONINFORMATION);
	//        sel.start+=4; //expand selection
	//        GuiSelectionSet(GUI_DUMP, &sel);

	//        GuiSelectionGet(GUI_STACK, &sel);
	//        sprintf(msg, "%p - %p", sel.start, sel.end);
	//        MessageBoxA(hwndDlg, msg, "Stack", MB_ICONINFORMATION);
	//        sel.start+=4; //expand selection
	//        GuiSelectionSet(GUI_STACK, &sel);
	//    }
	//    break;
}

//////////////////////////////////////////////////////////////////////////
// Load the Symbol modules
BOOL CALLBACK EnumSymProc(PSYMBOL_INFO pSymInfo, ULONG SymbolSize, PVOID UserContext)
{
	//printf("%08X %4u %s\n", pSymInfo->Address, SymbolSize, pSymInfo->Name);
	TCHAR szUndName[ 512 ];
	CHAR szName[512];
	
	//FillSymbolData(pSymInfo->Address,pSymInfo->Name,Load_Module_name);
	if (UnDecorateSymbolName(pSymInfo->Name, szUndName, sizeof(szUndName), UNDNAME_COMPLETE))
	{
		// UnDecorateSymbolName returned success
		memcpy((void*)szName,szUndName,sizeof(szUndName)); //Copy to another Variable for later check
		if (strlen(szName)!=0) //check if UnDecorateSymbolName is not empty
		{		
			if(strcmp(pSymInfo->Name,szName)== 0) // make compare in case there are Difference ,here we will use the UnDecorateSymbolName 
			{FillSymbolData(pSymInfo->Address,pSymInfo->Name,Load_Module_name);}
			else
			{FillSymbolData(pSymInfo->Address,szName,Load_Module_name);};
		}
		else
		{
			//something wrong
		}
	}
	
	return TRUE;
}
static void CBLOADDLL(CBTYPE cbType, void* callbackInfo)
{
	_plugin_logputs("[CBLOADDLL] debugging stopped!");
	//here for every loaded module we will get Symbol of this module and we will
	//fill this Symbol in GoTo Dialog to get them later
	DWORD oldSymOptions = SymGetOptions();
	SymSetOptions(SYMOPT_UNDNAME | SYMOPT_DEFERRED_LOADS); //this interferes with the debugger option :)
	PLUG_CB_LOADDLL* info=(PLUG_CB_LOADDLL*)callbackInfo;
	HANDLE hProcess;
	//DWORD64 BaseOfDll;
	char *Mask;
	DWORD  error;
	Load_Module_name=info->modname;
	Module_Path=info->modInfo->ImageName;
	hProcess = ((PROCESS_INFORMATION*)TitanGetProcessInformation())->hProcess;
	//BaseOfDll = 0;
	Mask = NULL;
	//Load info of Loaded Module Base Path Name
	Module_name_Path_Base(info->modInfo->BaseOfImage,info->modInfo->ImageName,info->modname,false);
	if (SymEnumSymbols(hProcess,info->modInfo->BaseOfImage, Mask, EnumSymProc, NULL))
	{
		// SymEnumSymbols succeeded		
	}
	else
	{
		// SymEnumSymbols failed
		printf("SymEnumSymbols failed: %d\n", GetLastError());
	}
	SymSetOptions(oldSymOptions); //restore the original options :)
}
static bool GoTODialog(int argc, char* argv[])
{
    if (!DbgIsDebugging())
	{return false;}	
	_plugin_logputs("[GoTODialog] Show GoTO Dialog!");
	GoToDialogInNewThread();
	/*duint x=GoToDialog();
	char cmd[50]="";
	sprintf(cmd, "disasm %p", x);
	DbgCmdExec(cmd);*/
    return true;	
}

//////////////////////////////////////////////////////////////////////////
//Go To Addrress dialog 
static bool GoTo_Addrress(int argc, char* argv[])
{
    if (!DbgIsDebugging())
	{return false;}	
	_plugin_logputs("[GoToAddrress] Show GoToAddrress Dialog!");
	GoToAddrress();	
    return true;	
}

//////////////////////////////////////////////////////////////////////////
//Titan 
static bool TitanHide(int argc, char* argv[])
{
    if (!DbgIsDebugging())
	{return false;}	
	_plugin_logputs("[GoTODialog] Show TitanHide_dialog!");
	int x=DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG1), NULL, (DLGPROC)DlgMain);
    return true;	
}
static BOOL CALLBACK DlgMain(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_INITDIALOG:
		{
			SetCheckBoxValue(hwndDlg);
		}
		return TRUE;

	case WM_CLOSE:
		{
			EndDialog(hwndDlg, 0);
		}
		return TRUE;

	case WM_COMMAND:
		{
			switch (LOWORD(wParam))
			{

				case ID_OK:
					{
						SetCheckBoxVar(hwndDlg);
						TitanHideCall(HidePid);
						EndDialog(hwndDlg, 0);
						return TRUE;
					}
				case ID_CANCEL:
					{
						EndDialog(hwndDlg, 0);
						return TRUE;
					}
				
			}
		}
		return TRUE;
	}
	return FALSE;
}

static void SetCheckBoxValue(HWND hwndDlg)
{
	if (CheckBox_PROCESSDEBUGFLAGS)
		CheckDlgButton(hwndDlg,IDC_CHK_PROCESSDEBUGFLAGS,BST_CHECKED);
	if (CheckBox_PROCESSDEBUGPORT)
		CheckDlgButton(hwndDlg,IDC_CHK_PROCESSDEBUGPORT,BST_CHECKED);
	if (CheckBox_PROCESSDEBUGOBJECTHANDLE)
		CheckDlgButton(hwndDlg,IDC_CHK_PROCESSDEBUGOBJECTHANDLE,BST_CHECKED);
	if (CheckBox_DEBUGOBJECT)
		CheckDlgButton(hwndDlg,IDC_CHK_DEBUGOBJECT,BST_CHECKED);
	if (CheckBox_SYSTEMDEBUGGERINFORMATION)
		CheckDlgButton(hwndDlg,IDC_CHK_SYSTEMDEBUGGERINFORMATION,BST_CHECKED);
	if (CheckBox_NTCLOSE)
		CheckDlgButton(hwndDlg,IDC_CHK_NTCLOSE,BST_CHECKED);
	if (CheckBox_THREADHIDEFROMDEBUGGER)
		CheckDlgButton(hwndDlg,IDC_CHK_THREADHIDEFROMDEBUGGER,BST_CHECKED);
	if (CheckBox_NTSETCONTEXTTHREAD)
		CheckDlgButton(hwndDlg,IDC_CHK_NTSETCONTEXTTHREAD,BST_CHECKED);
	
}
static void SetCheckBoxVar(HWND hwndDlg)
{
	if (IsDlgButtonChecked(hwndDlg, IDC_CHK_PROCESSDEBUGFLAGS))
		{
			CheckBox_PROCESSDEBUGFLAGS=true;
			BridgeSettingSet("TitanHide","IDC_CHK_PROCESSDEBUGFLAGS",bool_char(CheckBox_PROCESSDEBUGFLAGS));
		}
	else {CheckBox_PROCESSDEBUGFLAGS=false;
	BridgeSettingSet("TitanHide","IDC_CHK_PROCESSDEBUGFLAGS",bool_char(CheckBox_PROCESSDEBUGFLAGS));}
	//
	if (IsDlgButtonChecked(hwndDlg, IDC_CHK_PROCESSDEBUGPORT))
	{
		CheckBox_PROCESSDEBUGPORT=true;
		BridgeSettingSet("TitanHide","IDC_CHK_PROCESSDEBUGPORT",bool_char(CheckBox_PROCESSDEBUGPORT));
	}
	else {CheckBox_PROCESSDEBUGPORT=false;
	BridgeSettingSet("TitanHide","IDC_CHK_PROCESSDEBUGPORT",bool_char(CheckBox_PROCESSDEBUGPORT));}
	//
	if (IsDlgButtonChecked(hwndDlg, IDC_CHK_PROCESSDEBUGOBJECTHANDLE))
	{
		CheckBox_PROCESSDEBUGOBJECTHANDLE=true;
		BridgeSettingSet("TitanHide","IDC_CHK_PROCESSDEBUGOBJECTHANDLE",bool_char(CheckBox_PROCESSDEBUGOBJECTHANDLE));
	}
	else {CheckBox_PROCESSDEBUGOBJECTHANDLE=false;
	BridgeSettingSet("TitanHide","IDC_CHK_PROCESSDEBUGOBJECTHANDLE",bool_char(CheckBox_PROCESSDEBUGOBJECTHANDLE));}
	//
	if (IsDlgButtonChecked(hwndDlg, IDC_CHK_DEBUGOBJECT))
	{
		CheckBox_DEBUGOBJECT=true;
		BridgeSettingSet("TitanHide","IDC_CHK_DEBUGOBJECT",bool_char(CheckBox_DEBUGOBJECT));
	}
	else {CheckBox_DEBUGOBJECT=false;
	BridgeSettingSet("TitanHide","IDC_CHK_DEBUGOBJECT",bool_char(CheckBox_DEBUGOBJECT));}
	//
	if (IsDlgButtonChecked(hwndDlg, IDC_CHK_SYSTEMDEBUGGERINFORMATION))
	{
		CheckBox_SYSTEMDEBUGGERINFORMATION=true;
		BridgeSettingSet("TitanHide","IDC_CHK_SYSTEMDEBUGGERINFORMATION",bool_char(CheckBox_SYSTEMDEBUGGERINFORMATION));
	}
	else {CheckBox_SYSTEMDEBUGGERINFORMATION=false;
	BridgeSettingSet("TitanHide","IDC_CHK_SYSTEMDEBUGGERINFORMATION",bool_char(CheckBox_SYSTEMDEBUGGERINFORMATION));}
	//
	if (IsDlgButtonChecked(hwndDlg, IDC_CHK_NTCLOSE))
	{
		CheckBox_NTCLOSE=true;
		BridgeSettingSet("TitanHide","IDC_CHK_NTCLOSE",bool_char(CheckBox_NTCLOSE));
	}
	else {CheckBox_NTCLOSE=false;
	BridgeSettingSet("TitanHide","IDC_CHK_NTCLOSE",bool_char(CheckBox_NTCLOSE));}
	//
	if (IsDlgButtonChecked(hwndDlg, IDC_CHK_THREADHIDEFROMDEBUGGER))
	{
		CheckBox_THREADHIDEFROMDEBUGGER=true;
		BridgeSettingSet("TitanHide","IDC_CHK_THREADHIDEFROMDEBUGGER",bool_char(CheckBox_THREADHIDEFROMDEBUGGER));
	}
	else {CheckBox_THREADHIDEFROMDEBUGGER=false;
	BridgeSettingSet("TitanHide","IDC_CHK_THREADHIDEFROMDEBUGGER",bool_char(CheckBox_THREADHIDEFROMDEBUGGER));}
	//
	if (IsDlgButtonChecked(hwndDlg, IDC_CHK_NTSETCONTEXTTHREAD))
	{
		CheckBox_NTSETCONTEXTTHREAD=true;
		BridgeSettingSet("TitanHide","IDC_CHK_NTSETCONTEXTTHREAD",bool_char(CheckBox_NTSETCONTEXTTHREAD));
	}
	else {CheckBox_NTSETCONTEXTTHREAD=false;
	BridgeSettingSet("TitanHide","IDC_CHK_NTSETCONTEXTTHREAD",bool_char(CheckBox_NTSETCONTEXTTHREAD));};
	
}

static ULONG GetTypeDword()
{
	ULONG Option = 0;
	/*if (IsDlgButtonChecked(hwndDlg, IDC_CHK_PROCESSDEBUGFLAGS))
		Option |= (ULONG)HideProcessDebugFlags;
	if (IsDlgButtonChecked(hwndDlg, IDC_CHK_PROCESSDEBUGPORT))
		Option |= (ULONG)HideProcessDebugPort;
	if (IsDlgButtonChecked(hwndDlg, IDC_CHK_PROCESSDEBUGOBJECTHANDLE))
		Option |= (ULONG)HideProcessDebugObjectHandle;
	if (IsDlgButtonChecked(hwndDlg, IDC_CHK_DEBUGOBJECT))
		Option |= (ULONG)HideDebugObject;
	if (IsDlgButtonChecked(hwndDlg, IDC_CHK_SYSTEMDEBUGGERINFORMATION))
		Option |= (ULONG)HideSystemDebuggerInformation;
	if (IsDlgButtonChecked(hwndDlg, IDC_CHK_NTCLOSE))
		Option |= (ULONG)HideNtClose;
	if (IsDlgButtonChecked(hwndDlg, IDC_CHK_THREADHIDEFROMDEBUGGER))
		Option |= (ULONG)HideThreadHideFromDebugger;
	if (IsDlgButtonChecked(hwndDlg, IDC_CHK_NTSETCONTEXTTHREAD))
		Option |= (ULONG)HideNtSetContextThread;*/
	if (CheckBox_PROCESSDEBUGFLAGS)
		Option |= (ULONG)HideProcessDebugFlags;
	if (CheckBox_PROCESSDEBUGPORT)
		Option |= (ULONG)HideProcessDebugPort;
	if (CheckBox_PROCESSDEBUGOBJECTHANDLE)
		Option |= (ULONG)HideProcessDebugObjectHandle;
	if (CheckBox_DEBUGOBJECT)
		Option |= (ULONG)HideDebugObject;
	if (CheckBox_SYSTEMDEBUGGERINFORMATION)
		Option |= (ULONG)HideSystemDebuggerInformation;
	if (CheckBox_NTCLOSE)
		Option |= (ULONG)HideNtClose;
	if (CheckBox_THREADHIDEFROMDEBUGGER)
		Option |= (ULONG)HideThreadHideFromDebugger;
	if (CheckBox_NTSETCONTEXTTHREAD)
		Option |= (ULONG)HideNtSetContextThread;
	return Option;
}

//Create start Service
bool SysService()
{
	//const char* serviceName="";
	//const char* executablePath="";
	SC_HANDLE hManager=OpenSCManager(0, 0, SC_MANAGER_CREATE_SERVICE);
	if(hManager==INVALID_HANDLE_VALUE)
		return false;
	SC_HANDLE hService=CreateService(hManager, serviceName, serviceName,
		SERVICE_ALL_ACCESS,
		SERVICE_KERNEL_DRIVER,
		SERVICE_DEMAND_START,
		SERVICE_ERROR_NORMAL,
		SysPath,
		0, 0, 0, 0, 0);
	CloseServiceHandle(hManager);
	if(!hService)
		return false;
	CloseServiceHandle(hService);


	return true;
}
bool startService()
{
	SC_HANDLE hManager=OpenSCManager(0, 0, STANDARD_RIGHTS_REQUIRED);
	if(!hManager)
		return false;
	SC_HANDLE hService=OpenService(hManager, serviceName, SERVICE_START);
	CloseServiceHandle(hManager);
	if(!hService)
		return false;
	if(!StartService(hService, 0, 0))
	{
		CloseServiceHandle(hService);
		return false;
	}
	CloseServiceHandle(hService);
	return true;
}

static void TitanHideCall(HIDE_COMMAND Command)
{
	HANDLE hDevice = CreateFileA("\\\\.\\TitanHide", GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);
	if (hDevice == INVALID_HANDLE_VALUE)
	{
		MessageBoxA(hwndDlg, "Could not open TitanHide handle...", "Driver loaded?", MB_ICONERROR);
		return;
	}
	HIDE_INFO HideInfo;
	HideInfo.Command = Command;
	HideInfo.Pid =(ULONG) PID_Debuged_Process;// GetDlgItemInt(hwndDlg, PID_Debuged_Process, 0, FALSE);
	HideInfo.Type = GetTypeDword();
	DWORD written = 0;
	if (WriteFile(hDevice, &HideInfo, sizeof(HIDE_INFO), &written, 0))
		//MessageBoxA(hwndDlg, "Data written!", "Done", MB_ICONINFORMATION);
		int x=0;
	else
		MessageBoxA(hwndDlg, "WriteFile error...", "Unknown cause", MB_ICONINFORMATION);
	CloseHandle(hDevice);
}


