//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by X64_Dbg_Addition.rc
//
#define IDD_DIALOG1                     101
#define IDC_CHK_PROCESSDEBUGFLAGS       1001
#define IDC_CHK_PROCESSDEBUGPORT        1002
#define IDC_CHK_PROCESSDEBUGOBJECTHANDLE 1003
#define IDC_CHK_DEBUGOBJECT             1004
#define IDC_CHK_SYSTEMDEBUGGERINFORMATION 1005
#define IDC_CHK_NTCLOSE                 1006
#define IDC_CHK_THREADHIDEFROMDEBUGGER  1007
#define IDC_CHK_NTSETCONTEXTTHREAD      1008
#define ID_OK                           1009
#define ID_CANCEL                       1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
