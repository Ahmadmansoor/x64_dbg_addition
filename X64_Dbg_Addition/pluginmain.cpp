// dllmain.cpp : Defines the entry point for the DLL application.
#include "pluginmain.h"
#include "X64_Dbg_Addition.h"
#include "resource.h"
#include <commctrl.h>


#define plugin_name "x64_Dbg_Addition"
#define plugin_version 001
int pluginHandle;
HWND hwndDlg;
int hMenu;
int hMenuDisasm;
int hMenuDump;
int hMenuStack;
HINSTANCE hInst;

CHAR2BOOL Char2Bool;

DLL_EXPORT bool pluginit(PLUG_INITSTRUCT* initStruct)
{
	initStruct->pluginVersion=plugin_version;
	initStruct->sdkVersion=PLUG_SDKVERSION;
	strcpy(initStruct->pluginName, plugin_name);
	pluginHandle=initStruct->pluginHandle;
	AdditionInit(initStruct);
	return true;
}

DLL_EXPORT bool plugstop()
{
	AdditionStop();
	return true;
}

DLL_EXPORT void plugsetup(PLUG_SETUPSTRUCT* setupStruct)
{
	hwndDlg=setupStruct->hwndDlg;
	hMenu=setupStruct->hMenu;	
	hMenuDisasm = setupStruct->hMenuDisasm;
	hMenuDump = setupStruct->hMenuDump;
	hMenuStack = setupStruct->hMenuStack;
	AdditionSetup();
}

extern "C" DLL_EXPORT BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	
	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		{
			HMODULE ModuleHandle= LoadLibraryA("x64_dbg_VB.dll");
			FillSymbolData=(FILLSYMBOLDATA)GetProcAddress(ModuleHandle,"FillSymbolData");
			GoToDialog=(GOTODIALOG)GetProcAddress(ModuleHandle,"GoToDialog");
			GoToDialogInNewThread=(GOTODIALOGINNEWTHREAD)GetProcAddress(ModuleHandle,"GoToDialogInNewThread");
			CopySysFile=(COPYSYSFIlE)GetProcAddress(ModuleHandle,"CopySysFile");
			Char2Bool=(CHAR2BOOL)GetProcAddress(ModuleHandle,"Char2Bool");
			GoToAddrress=(GOTOADDRRESS)GetProcAddress(ModuleHandle,"GoToAddrress");
			Module_name_Path_Base=(MODULE_NAME_PATH_BASE)GetProcAddress(ModuleHandle,"Module_name_Path_Base");
			SetClipboard=(SETCLIPBOARD)GetProcAddress(ModuleHandle,"SetClipboard");
			API_Category_BP=(API_CATEGORY_BP)GetProcAddress(ModuleHandle,"API_Category_BP");
			GetBPOFList=(GETBPOFLIST)GetProcAddress(ModuleHandle,"GetBPOFList");
			//HexEditGetAddress=(HEXEDITGETADDRESS)GetProcAddress(ModuleHandle,"HexEditGetAddress") ;
			//FillCommand=(FILLCOMMAND)GetProcAddress(ModuleHandle,"FillCommand");
			//FoundCommand_Form=(FOUNDCOMMAND_FORM)GetProcAddress(ModuleHandle,"ShowFindCommandDialog");
			//EnableKeyRegForm=(ENABLEKEYREGFORM)GetProcAddress(ModuleHandle,"EnableKeyRegForm");
			//FindIntermodulerCall=(FINDINTERMODULERCALL)GetProcAddress(ModuleHandle,"FindIntermodulerCall");

			//////////////////////////////////////////////////////////////////////////
			hInst = hinstDLL; // get the handle of the Plug in dll for load resource ( dialog )
			//////////////////////////////////////////////////////////////////////////
			char temp;
			memcpy(&temp,"false",5);
			
			BridgeSettingGet("TitanHide","IDC_CHK_PROCESSDEBUGFLAGS",&temp);
			CheckBox_PROCESSDEBUGFLAGS=char_bool(&temp);
			BridgeSettingGet("TitanHide","IDC_CHK_PROCESSDEBUGPORT",&temp);
			CheckBox_PROCESSDEBUGPORT=char_bool(&temp);			
			BridgeSettingGet("TitanHide","IDC_CHK_PROCESSDEBUGOBJECTHANDLE",&temp);
			CheckBox_PROCESSDEBUGOBJECTHANDLE=char_bool(&temp);
			BridgeSettingGet("TitanHide","IDC_CHK_DEBUGOBJECT",&temp);
			CheckBox_DEBUGOBJECT=char_bool(&temp);
			BridgeSettingGet("TitanHide","IDC_CHK_SYSTEMDEBUGGERINFORMATION",&temp);
			CheckBox_SYSTEMDEBUGGERINFORMATION=char_bool(&temp);
			BridgeSettingGet("TitanHide","IDC_CHK_NTCLOSE",&temp);
			CheckBox_NTCLOSE=char_bool(&temp);
			BridgeSettingGet("TitanHide","IDC_CHK_THREADHIDEFROMDEBUGGER",&temp);
			CheckBox_THREADHIDEFROMDEBUGGER=char_bool(&temp);
			BridgeSettingGet("TitanHide","IDC_CHK_NTSETCONTEXTTHREAD",&temp);
			CheckBox_NTSETCONTEXTTHREAD=char_bool(&temp);


			
			//////////////////////////////////////////////////////////////////////////
			break;
		}
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		{
			BridgeSettingSet("TitanHide","IDC_CHK_PROCESSDEBUGFLAGS",bool_char(CheckBox_PROCESSDEBUGFLAGS));
		}

		break;
	}
	return TRUE;
	
}

