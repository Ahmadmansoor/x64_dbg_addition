#ifndef _x64_Dbg_Addition_H
#define _x64_Dbg_Addition_H

#include "pluginmain.h"
#include <string.h>
#include <iostream>

//menu identifiers
#define about 0
#define GotoAPI 1
#define TitanHide_Menu 2
#define submenuentry 3
#define GoToAddrress_Menu 4
//menu Disasm window
#define Disasm_Addition_GoToAPI 5
#define Disasm_Addition_TitanHide 6
#define Disasm_Addition_GoToAddrress 7
#define Disasm_Addition_CopyAddres_VA 8
#define Disasm_Addition_CopyAddres_RVA 9

//menu dump window
#define Dump_Addition_CopyAddres_VA 10
#define Dump_Addition_CopyAddres_RVA 11
#define NUllMenu 12
#define API_Category_BP_Menu 13



/////////

//functions
void AdditionInit(PLUG_INITSTRUCT* initStruct);
void AdditionStop();
void AdditionSetup();

//here we will Define the Functions of VB.net dll -Plugin supporter -
typedef void (__cdecl *FILLSYMBOLDATA)(ULONG64 Address,char* ApiName ,const char* Load_Module_name);
typedef duint (__cdecl *GOTODIALOG)();
typedef void(__cdecl *GOTODIALOGINNEWTHREAD)();
typedef const char* (__cdecl *COPYSYSFIlE)();
typedef bool (__cdecl *CHAR2BOOL)(char * input);
typedef void (__cdecl *GOTOADDRRESS)();
typedef void (__cdecl *MODULE_NAME_PATH_BASE)(ULONG64 Module_Base,const char* Module_Path,const char* Module_name,bool ClearData);
typedef void (__cdecl *SETCLIPBOARD)(ULONG64 Value);
typedef void (__cdecl *API_CATEGORY_BP)();
typedef void (__cdecl *GETBPOFLIST)(BRIDGEBP BP1);
//typedef void(__cdec3l *HEXEDITGETADDRESS)(char* Address,char* sizeToRead);
//typedef void(__cdecl *FILLCOMMAND)(duint Address, char* CompleteInstr_x);
//typedef void(__cdecl *FOUNDCOMMAND_FORM)();
//typedef void(__cdecl *ENABLEKEYREGFORM)();
//typedef void(__cdecl *FINDINTERMODULERCALL)(duint address);

extern FILLSYMBOLDATA FillSymbolData;
extern GOTODIALOG GoToDialog;
extern GOTODIALOGINNEWTHREAD GoToDialogInNewThread;
extern COPYSYSFIlE CopySysFile;
extern CHAR2BOOL Char2Bool;
extern GOTOADDRRESS GoToAddrress;
extern MODULE_NAME_PATH_BASE Module_name_Path_Base;
extern SETCLIPBOARD SetClipboard;
extern API_CATEGORY_BP API_Category_BP;
extern GETBPOFLIST GetBPOFList;
//extern HEXEDITGETADDRESS HexEditGetAddress;
//extern FILLCOMMAND FillCommand;
//extern FOUNDCOMMAND_FORM FoundCommand_Form;
//extern ENABLEKEYREGFORM EnableKeyRegForm;
//extern FINDINTERMODULERCALL FindIntermodulerCall;

//////////////////////////////////////////////////////////////////////////
// Variables for CheckBox in the Dialog
extern  bool CheckBox_PROCESSDEBUGFLAGS;
extern  bool CheckBox_PROCESSDEBUGPORT;
extern  bool CheckBox_PROCESSDEBUGOBJECTHANDLE;
extern  bool CheckBox_DEBUGOBJECT;
extern  bool CheckBox_SYSTEMDEBUGGERINFORMATION;
extern  bool CheckBox_NTCLOSE;
extern  bool CheckBox_THREADHIDEFROMDEBUGGER;
extern  bool CheckBox_NTSETCONTEXTTHREAD;




inline const char* bool_char(const bool b) {
	return b ? "true" : "false";
}

inline char* bool_charX(const bool b) {
	if (b)
		return "true";
	else
		return "false";

}

inline const bool char_bool(char* bx) {
	std::string Kl=(std::string)bx;
	int xx=Kl.find("true");

	if (xx==0)
		return true;
	else
		return false;

}


#endif //_X64_Dbg_Addition_H
