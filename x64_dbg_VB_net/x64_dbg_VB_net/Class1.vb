﻿Imports System.Runtime.InteropServices

Public Class x64_dbg_Class

    'This for Create Goto Dialog to go to the address of API's of Loaded Module's 
    'Get All Symbol of Loaded mudules in the process
    Public Class API_info
        Public Address As Int64
        Public APIName As String
        Public Load_Module_name As String
        'Public Module_Path As String
    End Class
    Public Class Module_name_Path_Pase_Class
        Public Module_Base As Int64
        Public Module_Path As String
        Public Module_name As String
    End Class
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> _
    Public Structure Disasm_INSTR
        Public Address As Int64
        Public CommanD As String
    End Structure
    Public Structure SELECTIONDATA
        Dim start_Select As Int64
        Dim End_Select As Int64
    End Structure



    Enum BPXTYPE
        bp_none = 0
        bp_normal = 1
        bp_hardware = 2
        bp_memory = 4
    End Enum
    Structure BPMAP
        Dim count As Int64
        Dim bp As Int64
    End Structure
    Structure BRIDGEBP
        Dim type As BPXTYPE
        Dim addr As Int64
        Dim enabled As Boolean
        Dim singleshoot As Boolean
        Dim active As Boolean
        Dim name As String
        Dim modx As String
        Dim slot As UInt64
    End Structure

End Class
