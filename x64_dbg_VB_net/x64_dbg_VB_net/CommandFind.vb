﻿Imports x64_dbg_VB_net.x64_dbg_VB_Module
Imports x64_dbg_VB_net.x64_dbg_Class

Public Class CommandFind
    Dim CommandList As List(Of String)
    Private Declare Function DbgCmdExec Lib "x64_bridge.dll" (ByVal cmd As String) As Boolean
    Private Declare Function DbgMemRead Lib "x64_bridge.dll" (ByVal Address As IntPtr, ByVal lpBuffer() As Byte, ByVal size_ofData As IntPtr) As Boolean
    Private Structure FindIntermoduler_Stru
        Dim Address_FM As Int64
        Dim Command_FM As String
        Dim APiName_FM As String
    End Structure
    Private Sub CommandFind_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Disasm_INSTR_List.Clear()
    End Sub
    Private Sub CommandFind_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'AsmCommand_DGV.Rows.Add(Disasm_INSTR_List.Count)
        'For i = 0 To Disasm_INSTR_List.Count - 1
        '    AsmCommand_DGV.Item(0, i).Value = Hex(Disasm_INSTR_List.Item(i).Address)
        '    AsmCommand_DGV.Item(1, i).Value = Disasm_INSTR_List.Item(i).CommanD.ToString

        'Next
        CommandList = (From s In Disasm_INSTR_List Select s.CommanD).ToList
        CommandSearch_CB.DataSource = CommandList.ToArray

    End Sub

    Private Sub TextBox1_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs)
        If (e.KeyCode = Windows.Forms.Keys.Enter) Then
            Dim g As List(Of Disasm_INSTR)
            g = (From s In Disasm_INSTR_List Where s.CommanD = CommandSearch_CB.Text.ToString Select s).ToList
            AsmCommand_DGV.Rows.Clear()
            If (g.Count = 0) Then Exit Sub
            AsmCommand_DGV.Rows.Add(g.Count)

            For i = 0 To g.Count - 1
                AsmCommand_DGV.Item(0, i).Value = Hex(g.Item(i).Address)
                AsmCommand_DGV.Item(1, i).Value = g.Item(i).CommanD.ToString

            Next
        End If
        'If CommandSearch_TB.Text = "" Then
        '    AsmCommand_DGV.Rows.Add(Disasm_INSTR_List.Count)
        '    For i = 0 To Disasm_INSTR_List.Count - 1
        '        AsmCommand_DGV.Item(0, i).Value = Hex(Disasm_INSTR_List.Item(i).Address)
        '        AsmCommand_DGV.Item(1, i).Value = Disasm_INSTR_List.Item(i).CommanD.ToString

        '    Next
        'End If
    End Sub


    Private Sub CommandSearch_CB_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles CommandSearch_CB.KeyUp
        If (e.KeyCode = Windows.Forms.Keys.Enter) Then
            'Dim g As List(Of Disasm_INSTR)
            Dim g = (From s In Disasm_INSTR_List Where s.CommanD = CommandSearch_CB.Text.ToString Select s).ToList
            AsmCommand_DGV.Rows.Clear()
            If g.Count = 0 Then Exit Sub
            If (g.Count = 0) Then Exit Sub
            AsmCommand_DGV.Rows.Add(g.Count)
            For i = 0 To g.Count - 1
                AsmCommand_DGV.Item(0, i).Value = Hex(g.Item(i).Address)
                AsmCommand_DGV.Item(1, i).Value = g.Item(i).CommanD.ToString

            Next
        End If
    End Sub

    Private Sub AsmCommand_DGV_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles AsmCommand_DGV.CellContentClick
        Dim GetrowIndex As Integer = e.RowIndex
        Dim GetAddress As String = AsmCommand_DGV.Item(0, e.RowIndex).Value.ToString
        Dim xx As Boolean = DbgCmdExec("disasm " + GetAddress)
    End Sub

    Private Sub FindIntermoduler_Bu_Click(sender As System.Object, e As System.EventArgs) Handles FindIntermoduler_Bu.Click
        'Dim FindIntermoduler_List As New List(Of FindIntermoduler_Stru)
        'Dim FindIntermoduler_List As Disasm_INSTR_List
        Dim FindIntermoduler_Stru_x As New FindIntermoduler_Stru
        Dim FindIntermoduler_List = From s In Disasm_INSTR_List Where s.CommanD.Contains("call")
        For Each x In FindIntermoduler_List
            Dim temp As String
            temp = x.CommanD

            Dim temp1 As Int64
            Try
                temp1 = Convert.ToInt64(Mid(temp, 5).Trim, 16)
                Dim d = (From b In API_info_List Where temp1 = b.Address).First
            Catch ex As Exception
                Dim FindBractic As Integer = temp.IndexOf("[")
                If FindBractic > 0 Then
                    Try
                        temp = Mid(temp, FindBractic + 2).Trim
                        temp = temp.Substring(1, temp.Length - 2)
                        temp1 = Convert.ToInt64(temp, 16)
                        Dim Readed_dat(7) As Byte
                        DbgMemRead(temp1, Readed_dat, 16)
                        temp = ""
                        For i = 0 To 7
                            Dim t As String = Hex(Readed_dat(i))
                            If t.Length < 2 Then t = "0" + t
                            temp = t + temp
                        Next
                        temp1 = Convert.ToInt64(temp, 16)
                        Dim ddd = From ss In API_info_List Where ss.Address = temp1
                        If Not (ddd Is Nothing) Then

                        End If
                        Dim xx As Integer
                        xx = xx + 1
                    Catch ex1 As Exception
                        Dim xx As Integer
                        xx = xx + 1
                    End Try
                End If

            End Try



        Next
    End Sub
End Class