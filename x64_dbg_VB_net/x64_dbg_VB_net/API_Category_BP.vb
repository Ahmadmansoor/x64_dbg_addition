﻿Imports System.Xml
Imports System.Windows.Forms
Imports System.Collections.Generic
Imports System.Collections
Imports System
Imports System.IO
Imports System.Runtime.Serialization
Imports System.Runtime
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Reflection
Imports System.Security.Permissions
Imports System.Xml.Serialization
Imports x64_dbg_VB_net.x64_dbg_Class
Imports System.Text.RegularExpressions

'https://msdn.microsoft.com/en-us/library/windows/desktop/ms725473%28v=vs.85%29.aspx
Public Class API_Category_BP
    Private NodeDataFile As String = IO.Path.Combine(Application.StartupPath, "plugins\API_Category_BP.xml")
    Private Declare Function DbgCmdExec Lib "x64_bridge.dll" (ByVal cmd As String) As Boolean
    Private Declare Function DbgGetBpxTypeAt Lib "x64_bridge.dll" (ByVal addr As Int64) As BPXTYPE
    Private Declare Function DbgGetBpList Lib "x64_bridge.dll" (ByVal type As BPXTYPE, ByRef list As BPMAP) As Integer
    Private Declare Sub BPList Lib "X64_Dbg_Addition.dp64" ()

    <Serializable()> _
    Public Class TNode
        Public tn As TreeNode
    End Class
    

    Private Sub SaveNodes(ByVal filePath As String)
        Dim XMLDoc As New Xml.XmlDocument
        Dim RootNode As TreeNode
        Dim NewXMLNode As Xml.XmlNode
        Dim XMLAttribute As Xml.XmlAttribute

        XMLDoc.LoadXml(("<?xml version='1.0' ?>" & _
        "<XMLTreeView>" & _
        "</XMLTreeView>"))
        For Each RootNode In TreeView_API.Nodes
            NewXMLNode = XMLDoc.CreateNode(Xml.XmlNodeType.Element, "Node",
            "Node", "")
            XMLAttribute = XMLDoc.CreateAttribute("name")
            XMLAttribute.Value = RootNode.Text

            NewXMLNode.Attributes.Append(XMLAttribute)
            XMLDoc.DocumentElement.AppendChild(NewXMLNode)

            SaveChildNodes(NewXMLNode, RootNode, XMLDoc)
        Next

        XMLDoc.Save(NodeDataFile)
    End Sub
    Private Sub SaveChildNodes(ByVal XMLNode As Xml.XmlNode, ByVal ParentNode As TreeNode, ByVal XMLDoc As Xml.XmlDocument)
        Dim ChildNode As TreeNode
        Dim NewXMLNode As Xml.XmlNode
        Dim XMLAttribute As Xml.XmlAttribute

        For Each ChildNode In ParentNode.Nodes
            NewXMLNode = XMLDoc.CreateNode(Xml.XmlNodeType.Element, "Node",
            "Node", "")
            XMLAttribute = XMLDoc.CreateAttribute("name")
            XMLAttribute.Value = ChildNode.Text

            NewXMLNode.Attributes.Append(XMLAttribute)
            XMLNode.AppendChild(NewXMLNode)

            SaveChildNodes(NewXMLNode, ChildNode, XMLDoc)
        Next

    End Sub
    Private Sub AddChildNodes(ByVal XMLNode As Xml.XmlNode, ByVal ParentNode As TreeNode)
        Dim ChildXMLNode As Xml.XmlNode
        Dim NewNode As TreeNode

        For Each ChildXMLNode In XMLNode.ChildNodes
            NewNode = ParentNode.Nodes.Add(ChildXMLNode.Attributes(0).Value)
            If ChildXMLNode.ChildNodes.Count > 0 Then
                AddChildNodes(ChildXMLNode, NewNode)
            End If
        Next

    End Sub

    'Loads the Treeview Nodes from the binary file
    Private Sub LoadNodes(ByVal filePath As String)
        Dim XMLDoc As New Xml.XmlDocument
        Dim XMLNode As Xml.XmlNode
        Dim ParentNode As TreeNode

        If TreeView_API Is Nothing Then
            Throw New ArgumentNullException("TreeView")
        End If

        Try
            XMLDoc.Load(NodeDataFile)
        Catch e As Exception
            Throw New Exception(e.Message)
        End Try

        For Each XMLNode In XMLDoc.DocumentElement.ChildNodes
            ParentNode = TreeView_API.Nodes.Add(XMLNode.Attributes(0).Value)
            If XMLNode.ChildNodes.Count > 0 Then
                AddChildNodes(XMLNode, ParentNode)
            End If
        Next
    End Sub

    Private Sub API_Category_BP_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F2 Then
            F2Pressed()
        ElseIf e.KeyCode = Keys.F3 Then
            F3Pressed()
        ElseIf e.KeyCode = Keys.Enter Then
            EnterKeyPressed1()
        End If
    End Sub
    Private Sub EnterKeyPressed()
        If TreeView_API.SelectedNode.Level = 0 Then Exit Sub
        Dim GetAPI As String = TreeView_API.SelectedNode.Text
        Dim APIName, Modulename As String
        'APIName = GetAPI.Substring(0, GetAPI.IndexOf("."))  'old
        'some API have look like this :
        'private: static void __cdecl QAbstractAnimation::qt_static_metacall(class QObject * __ptr64,enum QMetaObject::Call,int,void * __ptr64 * __ptr64)
        'which contain a lot of .
        'so we change it to  
        Dim tt As String = GetAPI
        Dim LastDotIndex As Integer = GetAPI.LastIndexOf(".")
        Do Until tt = "."
            LastDotIndex = LastDotIndex - 1
            tt = GetAPI.Substring(LastDotIndex - 1, 1)
        Loop
        APIName = GetAPI.Substring(0, LastDotIndex - 1)
        '
        Modulename = GetAPI.Substring(GetAPI.IndexOf(".") + 1, GetAPI.Length - GetAPI.IndexOf(".") - 1)
        Dim AddressOFAPI As Int64
        'For Symbols * and other ...search not return avalue so I do this way 
        If APIName.Contains("*") Then
            Dim FindAllHaveStarngeSymble = From j In API_info_List Where (j.APIName.Contains("*") And j.Load_Module_name = Modulename)
            For xx = 0 To FindAllHaveStarngeSymble.Count
                Dim ll As String = FindAllHaveStarngeSymble.ElementAt(xx).APIName.ToString.ToLower
                If (ll.ToLower = APIName) Then
                    Dim j = FindAllHaveStarngeSymble.ElementAt(xx).Address
                    DbgCmdExec("disasm " & Hex(j))
                    Exit For
                End If
            Next
        ElseIf APIName.Length > 18 Then

            Dim FindAllHaveStarngeSymble = From sss In API_info_List Where sss.Load_Module_name = "qtcore4.dll" Select sss
            For zz = 0 To FindAllHaveStarngeSymble.Count - 1
                If Regex.IsMatch(FindAllHaveStarngeSymble.ElementAt(zz).APIName.ToString, APIName) Then
                    Dim j = FindAllHaveStarngeSymble.ElementAt(zz).Address
                    DbgCmdExec("disasm " & Hex(j))
                End If
            Next
            'AddressOFAPI = (From j In API_info_List Where (j.APIName = APIName And j.Load_Module_name = Modulename) Select j.Address).First()
            'Dim ll = From k In API_info_List Where k.APIName.Contains(APIName) Select k
            'For xx1 = 0 To ll.Count - 1
            '    If (ll = APIName.ToLower) Then
            '        Dim j = API_info_List.ElementAt(xx1).Address
            '        DbgCmdExec("disasm " & Hex(j))
            '        Exit For
            '    End If
            'Next
        Else
            AddressOFAPI = (From j In API_info_List Where (j.APIName = APIName And j.Load_Module_name = Modulename) Select j.Address).First()
            DbgCmdExec("disasm " & Hex(AddressOFAPI))
        End If
    End Sub
    Private Sub EnterKeyPressed1()

        If TreeView_API.SelectedNode.Level = 0 Then Exit Sub
        Dim GetAPI As String = TreeView_API.SelectedNode.Text
        Dim APIName, Modulename As String
        'APIName = GetAPI.Substring(0, GetAPI.IndexOf("."))  'old
        'some API have look like this :
        'private: static void __cdecl QAbstractAnimation::qt_static_metacall(class QObject * __ptr64,enum QMetaObject::Call,int,void * __ptr64 * __ptr64)
        'which contain a lot of .
        'so we change it to  
        Dim tt As String = GetAPI
        Dim LastDotIndex As Integer = GetAPI.LastIndexOf(".")
        Do Until tt = "."
            LastDotIndex = LastDotIndex - 1
            tt = GetAPI.Substring(LastDotIndex - 1, 1)
        Loop
        APIName = GetAPI.Substring(0, LastDotIndex - 1)
        '

        Modulename = GetAPI.Substring(GetAPI.IndexOf(".") + 1, GetAPI.Length - GetAPI.IndexOf(".") - 1)
        Dim AddressOFAPI As Int64
        If APIName.Length > 20 Then
            Try
                AddressOFAPI = (From sss In API_info_List Where sss.APIName.ToLower = APIName And sss.Load_Module_name = Modulename Select sss.Address).First
                DbgCmdExec("disasm " & Hex(AddressOFAPI))
            Catch ex As Exception
                MsgBox("maybe Libarary not loaded", MsgBoxStyle.OkOnly, "Error")
            End Try
        Else
            Try
                AddressOFAPI = (From sss In API_info_List Where sss.APIName = APIName And sss.Load_Module_name = Modulename Select sss.Address).First
                DbgCmdExec("disasm " & Hex(AddressOFAPI))
            Catch ex1 As Exception
                MsgBox("maybe Libarary not loaded", MsgBoxStyle.OkOnly, "Error")
            End Try
            
        End If


    End Sub
    Private Sub F2Pressed()
        If TreeView_API.SelectedNode.Level = 0 Then Exit Sub
        Dim GetAPI As String = TreeView_API.SelectedNode.Text
        Dim APIName, Modulename As String
        APIName = GetAPI.Substring(0, GetAPI.IndexOf("."))
        Modulename = GetAPI.Substring(GetAPI.IndexOf(".") + 1, GetAPI.Length - GetAPI.IndexOf(".") - 1)
        Dim AddressOFAPI As Int64 = (From j In API_info_List Where (j.APIName = APIName And j.Load_Module_name = Modulename) Select j.Address).First
        Dim IsThereBP As BPXTYPE = DbgGetBpxTypeAt(AddressOFAPI)
        If IsThereBP = BPXTYPE.bp_none Then
            DbgCmdExec("bp " & Hex(AddressOFAPI)) 'set bp
            TreeView_API.SelectedNode.ForeColor = Drawing.Color.Red
        Else
            DbgCmdExec("bpc " & Hex(AddressOFAPI)) 'remove  bp
            TreeView_API.SelectedNode.ForeColor = Drawing.Color.Black
        End If
    End Sub
    Private Sub F3Pressed()
        If TreeView_API.SelectedNode.Level = 0 Then Exit Sub
        Dim GetAPI As String = TreeView_API.SelectedNode.Text
        Dim APIName, Modulename As String
        APIName = GetAPI.Substring(0, GetAPI.IndexOf("."))
        Modulename = GetAPI.Substring(GetAPI.IndexOf(".") + 1, GetAPI.Length - GetAPI.IndexOf(".") - 1)
        Dim AddressOFAPI As Int64 = (From j In API_info_List Where (j.APIName = APIName And j.Load_Module_name = Modulename) Select j.Address).First
        Dim IsThereBP As BPXTYPE = DbgGetBpxTypeAt(AddressOFAPI)
        If IsThereBP = BPXTYPE.bp_none Then
            DbgCmdExec("bph " & Hex(AddressOFAPI)) 'set bp
            TreeView_API.SelectedNode.ForeColor = Drawing.Color.Pink
        Else
            DbgCmdExec("bphc " & Hex(AddressOFAPI)) 'remove  bp
            TreeView_API.SelectedNode.ForeColor = Drawing.Color.Black
        End If
    End Sub


    Private Sub CheckBPState()
       BPList()
        For Each g In BP_OfList
            Dim gg As Int64 = g.addr
            Try
                For i = 0 To TreeView_API.Nodes.Count - 1
                    'Dim nodes As List(Of String) = New List(Of String)
                    'GetAllChildren(TreeView_API.Nodes(i), nodes)
                    For Each childNode As TreeNode In TreeView_API.Nodes(i).Nodes
                        'nodes.Add(childNode.Text)
                        Dim nodes As String = childNode.Text
                        Dim GetAPI As String = nodes
                        Dim APIName, Modulename As String
                        APIName = GetAPI.Substring(0, GetAPI.IndexOf("."))
                        Modulename = GetAPI.Substring(GetAPI.IndexOf(".") + 1, GetAPI.Length - GetAPI.IndexOf(".") - 1)
                        Dim AddressOFAPI As Int64 = (From j In API_info_List Where (j.APIName = APIName And j.Load_Module_name = Modulename) Select j.Address).First
                        If AddressOFAPI = gg Then
                            If g.type = BPXTYPE.bp_normal Then childNode.ForeColor = Drawing.Color.Red
                            If g.type = BPXTYPE.bp_hardware Then childNode.ForeColor = Drawing.Color.Pink
                        End If

                    Next
                Next
            Catch ex As Exception

            End Try
        
        Next

    End Sub
   

    Private Sub API_Category_BP_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        If IO.File.Exists(NodeDataFile) Then
            LoadNodes(NodeDataFile)
        End If
        'ComboBox1.DataSource = (From x In APIName_List Select x Distinct Order By x).ToArray
        Label1.Text = "F2= Soft Breck Point F3=Hardware Breck Point " + Environment.NewLine + "Enter to Fllow at Disasmble window"
        'Label1.Text = "F2= Soft Breck Point F3=Hardware Breck Point Enter to Fllow at Disasmble window"
        CheckBPState()
    End Sub
    Private Sub SaveToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SaveToolStripMenuItem.Click
        SaveNodes(NodeDataFile)
    End Sub

    Private Sub ToolStripTextBox_AddSubTree_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles ToolStripTextBox_AddSubTree.KeyUp
        If e.KeyCode = Keys.Enter Then
            If TreeView_API.SelectedNode Is Nothing Then
                TreeView_API.Nodes.Add(ToolStripTextBox_AddSubTree.Text)
                ContextMenuStrip1.Visible = False
            Else
                TreeView_API.SelectedNode.Nodes.Add(ToolStripTextBox_AddSubTree.Text)
                ContextMenuStrip1.Visible = False
            End If

        End If
    End Sub

    Private Sub ToolStripTextBox_rename_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles ToolStripTextBox_rename.KeyUp
        If e.KeyCode = Keys.Enter Then
            If TreeView_API.SelectedNode Is Nothing Then
                'TreeView_API.Nodes.Add(ToolStripTextBox1.Text)
                ContextMenuStrip1.Visible = False
            Else
                TreeView_API.SelectedNode.Text = ToolStripTextBox_rename.Text
                ContextMenuStrip1.Visible = False
            End If

        End If
    End Sub


    Private Sub ToolStripTextBoxAddMainTree_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles ToolStripTextBoxAddMainTree.KeyUp
        If e.KeyCode = Keys.Enter Then
            TreeView_API.Nodes.Add(ToolStripTextBoxAddMainTree.Text)
            ContextMenuStrip1.Visible = False
        End If
    End Sub

    Private Sub TreeView_API_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles TreeView_API.MouseUp
        Try
            If TreeView_API.SelectedNode.Level < 1 Then
                If (e.Button = Windows.Forms.MouseButtons.Right) Then
                    ContextMenuStrip1.Contains(Me)
                    ContextMenuStrip1.Visible = True
                    ContextMenuStrip1.Show()
                    ContextMenuStrip1.Left = Me.Location.X + e.X + 20
                    ContextMenuStrip1.Top = Me.Location.Y + e.Y + 50
                    For i = 0 To ContextMenuStrip1.Items.Count - 1
                        ContextMenuStrip1.Items(i).Enabled = True
                    Next i

                End If
            ElseIf TreeView_API.SelectedNode Is Nothing Then
                ContextMenuStrip1.Contains(Me)
                ContextMenuStrip1.Visible = True
                ContextMenuStrip1.Show()
                ContextMenuStrip1.Left = Me.Location.X + e.X + 20
                ContextMenuStrip1.Top = Me.Location.Y + e.Y + 50
                For i = 0 To ContextMenuStrip1.Items.Count - 1
                    ContextMenuStrip1.Items(i).Enabled = False
                Next i
                ContextMenuStrip1.Items(0).Enabled = True   'Enable Add Main Menu 
                ContextMenuStrip1.Items(4).Enabled = True   'Enable save 
            Else
                If TreeView_API.SelectedNode.Level <= 1 Then
                    If (e.Button = Windows.Forms.MouseButtons.Right) Then
                        ContextMenuStrip1.Contains(Me)
                        ContextMenuStrip1.Visible = True
                        ContextMenuStrip1.Show()
                        ContextMenuStrip1.Left = Me.Location.X + e.X + 20
                        ContextMenuStrip1.Top = Me.Location.Y + e.Y + 50
                        ContextMenuStrip1.Items(0).Enabled = False
                        ContextMenuStrip1.Items(1).Enabled = False
                    End If
                End If
            End If
        Catch ex As Exception
            'MsgBox(ex.ToString)
            If TreeView_API.SelectedNode.Level >= 1 Then Exit Sub
            ContextMenuStrip1.Contains(Me)
            ContextMenuStrip1.Visible = True
            ContextMenuStrip1.Show()
            ContextMenuStrip1.Left = Me.Location.X + e.X + 20
            ContextMenuStrip1.Top = Me.Location.Y + e.Y + 50
            For i = 0 To ContextMenuStrip1.Items.Count - 1
                ContextMenuStrip1.Items(i).Enabled = False
            Next i
            ContextMenuStrip1.Items(0).Enabled = True   'Enable Add Main Menu 
            ContextMenuStrip1.Items(4).Enabled = True   'Enable save
        End Try
    End Sub

    Private Sub EditToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles EditToolStripMenuItem.Click
        Dim retValue As Integer = MsgBox("Are u sure you want to delete this node", MsgBoxStyle.OkCancel, "Remove Node")
        If retValue = 2 Then Exit Sub
        If TreeView_API.SelectedNode Is Nothing Then Exit Sub
        TreeView_API.SelectedNode.Remove()
    End Sub
   
    Private Sub ToolStripTextBox_AddSubTree_DoubleClick(sender As Object, e As System.EventArgs) Handles ToolStripTextBox_AddSubTree.DoubleClick
        If ComboBox1.Text <> "" Then
            ToolStripTextBox_AddSubTree.Text = ComboBox1.Text
        Else
            ToolStripTextBox_AddSubTree.Text = ""
        End If
    End Sub


    Private Sub BuLoadCB_Click(sender As System.Object, e As System.EventArgs) Handles BuLoadCB.Click
        'ComboBox1.DataSource = (From x In APIName_List Select x Distinct Order By x).ToArray
        ComboBox1.DataSource = (From x In APIName_List Select x Distinct).ToArray ' I remove order by to keep its order by Library name
        Dim LengthOFLongSting As Integer = (From z In APIName_List Select z.Length > 200).Count
        If LengthOFLongSting > 0 Then ComboBox1.DropDownWidth = 800
    End Sub

    Private Sub BU_GO_Click(sender As System.Object, e As System.EventArgs) Handles BU_GO.Click
        Dim v(3) As String
        If TB_Find0.Text <> "" Then v(0) = TB_Find0.Text.ToLower.Trim Else v(0) = ""
        If TB_Find1.Text <> "" Then v(1) = TB_Find1.Text.ToLower.Trim Else v(1) = ""
        If TB_Find2.Text <> "" Then v(2) = TB_Find2.Text.ToLower.Trim Else v(2) = ""
        If TB_Find3.Text <> "" Then v(3) = TB_Find3.Text.ToLower.Trim Else v(3) = ""
        Dim CB_Data As System.String() = ComboBox1.DataSource
        Dim CB_Data_ToLower = From n In CB_Data Select n.ToLower
        Dim gg1 = From x In CB_Data_ToLower Select x Where x.ToString.Contains(v(0)) And x.ToString.Contains(v(1)) And x.ToString.Contains(v(2)) And x.ToString.Contains(v(3))
        CB_Result.Items.Clear()
        For Each g In gg1
            CB_Result.Items.Add(g)
        Next
        Dim LengthOFLongSting As Integer = (From z In gg1 Select z.Length > 200).Count
        If LengthOFLongSting > 0 Then CB_Result.DropDownWidth = 800
    End Sub

    Private Sub BU_Add_Click(sender As System.Object, e As System.EventArgs) Handles BU_Add.Click
        If CB_Result.Text = "" Then Exit Sub
        If TreeView_API.SelectedNode Is Nothing Then
            Exit Sub
        Else
            TreeView_API.SelectedNode.Nodes.Add(CB_Result.Text)
        End If
    End Sub
End Class
