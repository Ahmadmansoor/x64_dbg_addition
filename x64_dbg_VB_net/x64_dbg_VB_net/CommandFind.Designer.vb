﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CommandFind
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CommandSearch_CB = New System.Windows.Forms.ComboBox()
        Me.AsmCommand_DGV = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FindComm_TCo = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.FindIntermoduler_DG = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FindIntermoduler_Bu = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.AsmCommand_DGV, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FindComm_TCo.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.FindIntermoduler_DG, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(17, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 19)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Command:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.FindComm_TCo)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(453, 322)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'CommandSearch_CB
        '
        Me.CommandSearch_CB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.CommandSearch_CB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.CommandSearch_CB.FormattingEnabled = True
        Me.CommandSearch_CB.Location = New System.Drawing.Point(119, 18)
        Me.CommandSearch_CB.Name = "CommandSearch_CB"
        Me.CommandSearch_CB.Size = New System.Drawing.Size(297, 21)
        Me.CommandSearch_CB.TabIndex = 3
        '
        'AsmCommand_DGV
        '
        Me.AsmCommand_DGV.AllowUserToAddRows = False
        Me.AsmCommand_DGV.AllowUserToDeleteRows = False
        Me.AsmCommand_DGV.AllowUserToResizeColumns = False
        Me.AsmCommand_DGV.AllowUserToResizeRows = False
        Me.AsmCommand_DGV.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.AsmCommand_DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.AsmCommand_DGV.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2})
        Me.AsmCommand_DGV.Location = New System.Drawing.Point(14, 45)
        Me.AsmCommand_DGV.Name = "AsmCommand_DGV"
        Me.AsmCommand_DGV.RowHeadersWidth = 30
        Me.AsmCommand_DGV.Size = New System.Drawing.Size(402, 168)
        Me.AsmCommand_DGV.TabIndex = 2
        '
        'Column1
        '
        Me.Column1.HeaderText = "Address"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 125
        '
        'Column2
        '
        Me.Column2.HeaderText = "Command"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 245
        '
        'FindComm_TCo
        '
        Me.FindComm_TCo.Controls.Add(Me.TabPage1)
        Me.FindComm_TCo.Controls.Add(Me.TabPage2)
        Me.FindComm_TCo.Location = New System.Drawing.Point(7, 19)
        Me.FindComm_TCo.Name = "FindComm_TCo"
        Me.FindComm_TCo.SelectedIndex = 0
        Me.FindComm_TCo.Size = New System.Drawing.Size(438, 295)
        Me.FindComm_TCo.TabIndex = 4
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.AsmCommand_DGV)
        Me.TabPage1.Controls.Add(Me.CommandSearch_CB)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(430, 269)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.FindIntermoduler_Bu)
        Me.TabPage2.Controls.Add(Me.FindIntermoduler_DG)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(430, 269)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'FindIntermoduler_DG
        '
        Me.FindIntermoduler_DG.AllowUserToAddRows = False
        Me.FindIntermoduler_DG.AllowUserToDeleteRows = False
        Me.FindIntermoduler_DG.AllowUserToResizeColumns = False
        Me.FindIntermoduler_DG.AllowUserToResizeRows = False
        Me.FindIntermoduler_DG.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.FindIntermoduler_DG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.FindIntermoduler_DG.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.Column3})
        Me.FindIntermoduler_DG.Location = New System.Drawing.Point(6, 6)
        Me.FindIntermoduler_DG.Name = "FindIntermoduler_DG"
        Me.FindIntermoduler_DG.RowHeadersWidth = 25
        Me.FindIntermoduler_DG.Size = New System.Drawing.Size(418, 225)
        Me.FindIntermoduler_DG.TabIndex = 3
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Address"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Command"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 170
        '
        'Column3
        '
        Me.Column3.HeaderText = "APIName"
        Me.Column3.Name = "Column3"
        Me.Column3.Width = 120
        '
        'FindIntermoduler_Bu
        '
        Me.FindIntermoduler_Bu.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FindIntermoduler_Bu.Location = New System.Drawing.Point(140, 236)
        Me.FindIntermoduler_Bu.Name = "FindIntermoduler_Bu"
        Me.FindIntermoduler_Bu.Size = New System.Drawing.Size(150, 27)
        Me.FindIntermoduler_Bu.TabIndex = 4
        Me.FindIntermoduler_Bu.Text = "Find"
        Me.FindIntermoduler_Bu.UseVisualStyleBackColor = True
        '
        'CommandFind
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(462, 331)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "CommandFind"
        Me.Text = "CommandFind"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.AsmCommand_DGV, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FindComm_TCo.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.FindIntermoduler_DG, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents AsmCommand_DGV As System.Windows.Forms.DataGridView
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CommandSearch_CB As System.Windows.Forms.ComboBox
    Friend WithEvents FindComm_TCo As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents FindIntermoduler_DG As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FindIntermoduler_Bu As System.Windows.Forms.Button
End Class
