﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GoTo_Address
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TB_UserAddress = New System.Windows.Forms.TextBox()
        Me.RB_VA = New System.Windows.Forms.RadioButton()
        Me.RB_RVA = New System.Windows.Forms.RadioButton()
        Me.CB_LoadedModule = New System.Windows.Forms.ComboBox()
        Me.BU_GOTO = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'TB_UserAddress
        '
        Me.TB_UserAddress.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TB_UserAddress.Location = New System.Drawing.Point(12, 12)
        Me.TB_UserAddress.Name = "TB_UserAddress"
        Me.TB_UserAddress.Size = New System.Drawing.Size(375, 27)
        Me.TB_UserAddress.TabIndex = 0
        '
        'RB_VA
        '
        Me.RB_VA.AutoSize = True
        Me.RB_VA.Checked = True
        Me.RB_VA.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RB_VA.Location = New System.Drawing.Point(67, 45)
        Me.RB_VA.Name = "RB_VA"
        Me.RB_VA.Size = New System.Drawing.Size(123, 22)
        Me.RB_VA.TabIndex = 1
        Me.RB_VA.TabStop = True
        Me.RB_VA.Text = "VA_Addrress"
        Me.RB_VA.UseVisualStyleBackColor = True
        '
        'RB_RVA
        '
        Me.RB_RVA.AutoSize = True
        Me.RB_RVA.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RB_RVA.Location = New System.Drawing.Point(204, 45)
        Me.RB_RVA.Name = "RB_RVA"
        Me.RB_RVA.Size = New System.Drawing.Size(128, 22)
        Me.RB_RVA.TabIndex = 2
        Me.RB_RVA.Text = "RVA_Addrees"
        Me.RB_RVA.UseVisualStyleBackColor = True
        '
        'CB_LoadedModule
        '
        Me.CB_LoadedModule.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_LoadedModule.DropDownWidth = 500
        Me.CB_LoadedModule.FormattingEnabled = True
        Me.CB_LoadedModule.Location = New System.Drawing.Point(12, 73)
        Me.CB_LoadedModule.Name = "CB_LoadedModule"
        Me.CB_LoadedModule.Size = New System.Drawing.Size(375, 21)
        Me.CB_LoadedModule.TabIndex = 3
        Me.CB_LoadedModule.Visible = False
        '
        'BU_GOTO
        '
        Me.BU_GOTO.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BU_GOTO.Location = New System.Drawing.Point(136, 108)
        Me.BU_GOTO.Name = "BU_GOTO"
        Me.BU_GOTO.Size = New System.Drawing.Size(115, 27)
        Me.BU_GOTO.TabIndex = 4
        Me.BU_GOTO.Text = "GO"
        Me.BU_GOTO.UseVisualStyleBackColor = True
        '
        'GoTo_Address
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(399, 144)
        Me.Controls.Add(Me.BU_GOTO)
        Me.Controls.Add(Me.CB_LoadedModule)
        Me.Controls.Add(Me.RB_RVA)
        Me.Controls.Add(Me.RB_VA)
        Me.Controls.Add(Me.TB_UserAddress)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "GoTo_Address"
        Me.Text = "GoTo_Address"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TB_UserAddress As System.Windows.Forms.TextBox
    Friend WithEvents RB_VA As System.Windows.Forms.RadioButton
    Friend WithEvents RB_RVA As System.Windows.Forms.RadioButton
    Friend WithEvents CB_LoadedModule As System.Windows.Forms.ComboBox
    Friend WithEvents BU_GOTO As System.Windows.Forms.Button
End Class
