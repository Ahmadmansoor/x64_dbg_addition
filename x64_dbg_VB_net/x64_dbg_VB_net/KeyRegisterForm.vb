﻿Imports System.Runtime.InteropServices
Imports System.Windows.Forms
Imports x64_dbg_VB_net.x64_dbg_Class

Public Class KeyRegisterForm
    Private Declare Function GuiSelectionGet Lib "x64_bridge.dll" (ByVal hWindow As Integer, ByRef selection As SELECTIONDATA) As Boolean
    Private Declare Function DbgMemRead Lib "x64_bridge.dll" (ByVal Address As IntPtr, ByVal lpBuffer() As Byte, ByVal size_ofData As IntPtr) As Boolean
    Public Const MOD_ALT As Integer = &H1 '+ &H2 + &H4
    Public Const MOD_Ctrl As Integer = &H2
    Public Const MOD_Shift As Integer = &H4
    'H1 is MOD_ALT, H2 is MOD_CONTROL, H4 is MOD_SHIFT
    Public Const WM_HOTKEY As Integer = &H312
    Public Const GUI_DUMP = 1

    <DllImport("User32.dll")> _
    Public Shared Function RegisterHotKey(ByVal hwnd As IntPtr, _
                        ByVal id As Integer, ByVal fsModifiers As Integer, _
                        ByVal vk As Integer) As Integer
    End Function
    <DllImport("User32.dll")> _
    Public Shared Function UnregisterHotKey(ByVal hwnd As IntPtr, _
                        ByVal id As Integer) As Integer
    End Function

    Private Sub KeyRegisterForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        
        RegisterHotKey(Me.Handle, 100, MOD_ALT, Keys.C)
        Me.Visible = True
        Me.Hide()
        Me.Width = 0
        Me.Height = 0
    End Sub

    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
        If m.Msg = WM_HOTKEY Then
            Dim id As IntPtr = m.WParam
            Select Case (id.ToString)
                Case "100"
                    Dim selection As New SELECTIONDATA
                    If GuiSelectionGet(GUI_DUMP, selection) <> False Then
                        'DbgMemRead(
                    End If
                    
            End Select
        End If
        MyBase.WndProc(m)
    End Sub
End Class