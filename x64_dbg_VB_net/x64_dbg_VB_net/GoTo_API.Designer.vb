﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GoTo_API
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Goto_b = New System.Windows.Forms.Button()
        Me.API_CB = New System.Windows.Forms.ComboBox()
        Me.GoToDump_Bu = New System.Windows.Forms.Button()
        Me.BULoadCB = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Goto_b
        '
        Me.Goto_b.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Goto_b.Location = New System.Drawing.Point(12, 45)
        Me.Goto_b.Name = "Goto_b"
        Me.Goto_b.Size = New System.Drawing.Size(150, 34)
        Me.Goto_b.TabIndex = 3
        Me.Goto_b.Text = "GoTo CPU"
        Me.Goto_b.UseVisualStyleBackColor = True
        '
        'API_CB
        '
        Me.API_CB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.API_CB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.API_CB.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.API_CB.FormattingEnabled = True
        Me.API_CB.Location = New System.Drawing.Point(12, 12)
        Me.API_CB.Name = "API_CB"
        Me.API_CB.Size = New System.Drawing.Size(263, 27)
        Me.API_CB.TabIndex = 2
        '
        'GoToDump_Bu
        '
        Me.GoToDump_Bu.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GoToDump_Bu.Location = New System.Drawing.Point(194, 45)
        Me.GoToDump_Bu.Name = "GoToDump_Bu"
        Me.GoToDump_Bu.Size = New System.Drawing.Size(150, 34)
        Me.GoToDump_Bu.TabIndex = 4
        Me.GoToDump_Bu.Text = "GoTo Dump"
        Me.GoToDump_Bu.UseVisualStyleBackColor = True
        '
        'BULoadCB
        '
        Me.BULoadCB.Location = New System.Drawing.Point(281, 14)
        Me.BULoadCB.Name = "BULoadCB"
        Me.BULoadCB.Size = New System.Drawing.Size(63, 23)
        Me.BULoadCB.TabIndex = 5
        Me.BULoadCB.Text = "Load"
        Me.BULoadCB.UseVisualStyleBackColor = True
        '
        'GoTo_API
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(357, 89)
        Me.Controls.Add(Me.BULoadCB)
        Me.Controls.Add(Me.GoToDump_Bu)
        Me.Controls.Add(Me.Goto_b)
        Me.Controls.Add(Me.API_CB)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "GoTo_API"
        Me.Text = "GoTo_API"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Goto_b As System.Windows.Forms.Button
    Friend WithEvents API_CB As System.Windows.Forms.ComboBox
    Friend WithEvents GoToDump_Bu As System.Windows.Forms.Button
    Friend WithEvents BULoadCB As System.Windows.Forms.Button
End Class
