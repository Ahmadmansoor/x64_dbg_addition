﻿Imports System
Imports System.Linq
Imports System.Runtime.InteropServices
Imports RGiesecke.DllExport
Imports System.Windows.Forms
'Imports Elmah
Imports x64_dbg_VB_net.x64_dbg_Class


Module x64_dbg_VB_Module
    'Private Declare Sub DbgDisasmAt Lib "x64_bridge.dll" (ByVal addr As Int64, ByVal instr As DISASM_INSTR_VB)
    Private Declare Function DbgIsDebugging Lib "x64_bridge.dll" () As Boolean

    'This for Create Goto Dialog to go to the address of API's of Loaded Module's 
    'Get All Symbol of Loaded mudules in the process
    Public API_info_List As New List(Of API_info)
    Public APIName_List As New List(Of String)
    Public Module_name_Path_Base_List As New List(Of Module_name_Path_Pase_Class)
    Public Get_API_Address As Int64
    Public Hex_Editor_Address As Int64
    Public hex_byte_count As Integer

    Public Disasm_INSTR_List As New List(Of Disasm_INSTR)
    Public BP_OfList As New List(Of BRIDGEBP)

    '////////////////////////////////////////////////////////////////////////////
    '<DllExport("GoToDialog")> _
    'Public Function GoToDialog() As Int64
    '    'this will Create a Dialog in same Thread so the program will
    '    'wait it is answer Int64
    '    Dim NewVBForm As New GoTo_Dialog
    '    NewVBForm.ShowDialog()
    '    Return Get_API_Address
    'End Function
    Public Sub GoToForm()
        'this will Create a Dialog in same Thread so the program will
        'wait it is answer Int64
        If Not (DbgIsDebugging) Then Exit Sub
        Dim NewVBForm As New GoTo_API
        NewVBForm.ShowDialog()
        'Return Get_API_Address
    End Sub
    <DllExport("GoToDialogInNewThread")> _
    Public Sub GoToDialogInNewThread()
        'this will Create a Dialog in separate Thread so the program will
        'not wait it to answer Int64
        Dim thread As New Threading.Thread(AddressOf GoToForm)
        thread.SetApartmentState(Threading.ApartmentState.STA)
        thread.Start()
    End Sub
    Public Sub GoToAddrressForum()
        'this will Create a Dialog in same Thread so the program will
        'wait it is answer Int64
        If Not (DbgIsDebugging) Then Exit Sub
        Dim NewVBForm As New GoTo_Address
        NewVBForm.ShowDialog()
        'Return Get_API_Address
    End Sub

    <DllExport("SetClipboard")> _
    Public Sub SetClipboard(ByVal Value As Int64)
        Clipboard.SetText(Hex(Value))
    End Sub
    <DllExport("GoToAddrress")> _
    Public Sub GoToAddrress()
        'this will Create a Dialog in separate Thread so the program will
        'not wait it to answer Int64
        Dim thread As New Threading.Thread(AddressOf GoToAddrressForum)
        thread.SetApartmentState(Threading.ApartmentState.STA)
        thread.Start()
    End Sub
    <DllExport("FillSymbolData")>
    Public Sub FillSymbolData(ByVal Address As Int64, ByVal APIName As String, ByVal Load_Module_name As String)
        Dim g As New API_info 'define new class
        g.Address = Address
        g.APIName = APIName
        If Load_Module_name = "qtcore4.dll" Then
            Dim xx As Integer = 5
        End If
        g.Load_Module_name = Load_Module_name
        'g.Module_Path = Module_Path
        API_info_List.Add(g) 'add new item to API_info_List
        APIName_List.Add(APIName & "." & Load_Module_name) 'add new item to APIName_List
    End Sub
    <DllExport("Module_name_Path_Base")>
    Public Sub Module_name_Path_Base(ByVal Module_Base As Int64, ByVal Module_Path As String, ByVal Module_name As String, ByVal ClearData As Boolean)
        If ClearData = True Then ' First load of exe file so we clear all data needed
            Module_name_Path_Base_List.Clear()
            APIName_List.Clear()
            API_info_List.Clear()
            Module_name_Path_Base_List.Clear()
            Disasm_INSTR_List.Clear()
            BP_OfList.Clear()
        End If
        Dim g As New Module_name_Path_Pase_Class 'define new class
        g.Module_Base = Module_Base
        g.Module_Path = Module_Path
        g.Module_name = Module_name
        'g.Module_Path = Module_Path
        Module_name_Path_Base_List.Add(g) 'add new item to Module_name_Bath_Base_List
    End Sub

    '/////////////////////////////////////////////////////////////
    'FounfCommand
    <DllExport("FillCommand")>
    Public Sub FillCommand(ByVal Address As Int64, ByVal CompleteInstr As String)
        Dim g As New Disasm_INSTR
        g.Address = Address
        g.CommanD = CompleteInstr
        Disasm_INSTR_List.Add(g)

    End Sub
    <DllExport("ShowFindCommandDialog")>
    Public Sub ShowFindCommandDialog()
        Dim Thread As New Threading.Thread(AddressOf FinDCommand_Form)
        Thread.SetApartmentState(Threading.ApartmentState.STA)
        Thread.Start()
    End Sub
    Public Sub FinDCommand_Form()
        Dim FoundCommand_Form As New x64_dbg_VB_net.CommandFind
        FoundCommand_Form.ShowDialog()
    End Sub
    '////////////////////////////////////////////////////////////////
    'EnableKeyReg
    <DllExport("EnableKeyRegForm")>
    Public Sub EnableKeyRegForm()
        Dim Thread As New Threading.Thread(AddressOf EnableKeyReg_Form)
        Thread.SetApartmentState(Threading.ApartmentState.STA)
        Thread.Start()
    End Sub
    Public Sub EnableKeyReg_Form()
        Dim FoundCommand_Form As New x64_dbg_VB_net.KeyRegisterForm
        FoundCommand_Form.ShowDialog()
        FoundCommand_Form.Hide()
    End Sub
    '/////////////////////////////////////////////////////////////////
    Public Sub APICategoryApiForm()
        'this will Create a Dialog in same Thread so the program will
        'wait it is answer Int64
        If Not (DbgIsDebugging) Then Exit Sub
        Dim NewVBForm As New API_Category_BP
        NewVBForm.ShowDialog()
        'Return Get_API_Address
    End Sub
    <DllExport("API_Category_BP")> _
    Public Sub API_Category_BPNewThread()
        Dim thread As New Threading.Thread(AddressOf APICategoryApiForm)
        thread.SetApartmentState(Threading.ApartmentState.STA)
        thread.Start()
        'Dim NNN As New API_Category_BP
        'NNN.Show()
    End Sub
    '////////////////////////////////////////////////////////////////
    'Find Refrense Call
    <DllExport("FindIntermodulerCall")>
    Public Sub FindIntermodulerCall(ByVal address As Int64)
        'Dim DISASM_INSTR_VB_x As New DISASM_INSTR
        'ReDim DISASM_INSTR_VB_x.arg(2)
        ''ReDim DISASM_INSTR_VB_x.instruction(64)
        'Try
        '    DbgDisasmAt(address, DISASM_INSTR_VB_x)
        'Catch ex As Exception
        '    MsgBox(ex.ToString)
        'End Try
        Dim Thread As New Threading.Thread(AddressOf FinDCommand_Form)
        Thread.SetApartmentState(Threading.ApartmentState.STA)
        Thread.Start()
    End Sub

    '////////////////////////////////////////////////////////////////
    'Find Refrense Call
    <DllExport("CopySysFile")>
    Public Function CopySysFile() As String
        Dim SysPath As String = Environment.CurrentDirectory.ToString + "\plugins\TitanHide.sys"
        If (IO.File.Exists(Environment.SystemDirectory + "\drivers\TitanHide.sys")) Then
            Return Environment.SystemDirectory + "\drivers\TitanHide.sys"
        Else
            Try
                IO.File.Copy(SysPath, Environment.SystemDirectory + "\drivers\TitanHide.sys")
                Return Environment.SystemDirectory + "\drivers\TitanHide.sys"
            Catch ex As Exception
                MsgBox(ex.Message.ToString, MsgBoxStyle.OkOnly, "Error")
                Return ""
            End Try

        End If
    End Function

    '////////////////////////////////////////////////////////////////
    'char* to bool
    <DllExport("Char2Bool")>
    Public Function Char2Bool(ByVal input As String) As Boolean
        If input = "true" Then
            Return True
        Else
            Return False
        End If
    End Function
    '///////////////////////////////////////////////////////////////

    <DllExport("GetBPOFList")> _
    Public Sub GetBPOFList(ByVal BP1 As BRIDGEBP)
        BP_OfList.Add(BP1)
    End Sub
End Module
