﻿Imports x64_dbg_VB_net.x64_dbg_VB_Module
Public Class GoTo_Address
    Const GUI_DUMP = 1
    Const GUI_DISASSEMBLY = 0
    Private Declare Function DbgCmdExec Lib "x64_bridge.dll" (ByVal cmd As String) As Boolean
    Private Sub GoTo_Address_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim gg = (From x In Module_name_Path_Base_List Select x.Module_Path).ToList
        CB_LoadedModule.DataSource = gg
        For Each dd In gg
            If dd.Length > 120 Then
                CB_LoadedModule.DropDownWidth = 700
            End If
        Next

    End Sub

    Private Sub RB_VA_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles RB_VA.CheckedChanged
        If (RB_VA.Checked) Then
            CB_LoadedModule.Visible = False
        Else
            CB_LoadedModule.Visible = True
        End If
    End Sub

    Private Sub RB_RVA_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles RB_RVA.CheckedChanged
        If (RB_RVA.Checked) Then
            CB_LoadedModule.Visible = True
        Else
            CB_LoadedModule.Visible = False
        End If
    End Sub

    Private Sub BU_GOTO_Click(sender As System.Object, e As System.EventArgs) Handles BU_GOTO.Click
        Dim JustThisString As String() = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F", "a", "b", "c", "d", "e", "f"}
        If TB_UserAddress.Text = "" Then Exit Sub
        'If TB_UserAddress.Text.Length <> 16 Then Exit Sub
        For Each x In TB_UserAddress.Text
            For Each d In JustThisString
                If x = d Then GoTo Good
            Next
            MsgBox("Error Address", MsgBoxStyle.OkOnly, "Error") : Exit Sub
Good:
        Next
        If (RB_VA.Checked) Then
            DbgCmdExec("disasm " + TB_UserAddress.Text)
        ElseIf (RB_RVA.Checked) Then
            Dim ModulePath As String = CB_LoadedModule.Text
            Dim NewAddrress As Int64 = (From xz In Module_name_Path_Base_List Where xz.Module_Path.Contains(CB_LoadedModule.Text) Select xz.Module_Base).First
            NewAddrress = NewAddrress + Convert.ToUInt64(TB_UserAddress.Text, 16)
            Dim NewAddrress_str As String = Hex(NewAddrress)
            DbgCmdExec("disasm " + NewAddrress_str)
        End If
    End Sub

    'Private Sub TB_UserAddress_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles TB_UserAddress.KeyUp
    '    'Dim temp As String = String.Empty
    '    'For Each s In TB_UserAddress.Text
    '    '    ' temp = temp + s.ToString.ToUpper
    '    'Next
    '    '' TB_UserAddress.Text = temp
    'End Sub
End Class