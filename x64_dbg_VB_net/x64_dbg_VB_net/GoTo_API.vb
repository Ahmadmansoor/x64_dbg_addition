﻿Imports System.Threading
Imports x64_dbg_VB_net.x64_dbg_Class

'http://webcache.googleusercontent.com/search?q=cache:qICcqtwblxIJ:msdn.microsoft.com/en-us/library/ff649143.aspx+&cd=8&hl=en&ct=clnk
Public Class GoTo_API
    Const GUI_DUMP = 1
    Const GUI_DISASSEMBLY = 0
    Private Declare Function DbgCmdExec Lib "x64_bridge.dll" (ByVal cmd As String) As Boolean


    Private Sub Goto_b_Click(sender As System.Object, e As System.EventArgs) Handles Goto_b.Click
        Dim APIName As String
        Dim Modulename As String
        Dim GetAPI As String = API_CB.Text
        Dim tt As String = GetAPI
        Dim LastDotIndex As Integer = GetAPI.LastIndexOf(".")
        Do Until tt = "."
            LastDotIndex = LastDotIndex - 1
            tt = GetAPI.Substring(LastDotIndex - 1, 1)
        Loop
        APIName = GetAPI.Substring(0, LastDotIndex - 1)
        Modulename = GetAPI.Substring(GetAPI.IndexOf(".") + 1, GetAPI.Length - GetAPI.IndexOf(".") - 1)
        Dim AddressOFAPI As Int64
        'For Symbols * and other ...search not return avalue so I do this way 
        If APIName.Contains("*") Then
            Dim FindAllHaveStarngeSymble = From j In API_info_List Where (j.APIName.Contains("*") And j.Load_Module_name = Modulename)
            For xx = 0 To FindAllHaveStarngeSymble.Count - 1
                Dim ll As String = FindAllHaveStarngeSymble.ElementAt(xx).APIName.ToString.ToLower
                If (ll.ToLower = APIName) Then
                    Dim j = FindAllHaveStarngeSymble.ElementAt(xx).Address
                    DbgCmdExec("disasm " & Hex(j))
                    Exit For
                End If
            Next
        Else
            AddressOFAPI = (From j In API_info_List Where (j.APIName = APIName And j.Load_Module_name = Modulename) Select j.Address).First()
            DbgCmdExec("disasm " & Hex(AddressOFAPI))
        End If
        'Dim APIName As String = API_CB.Text.Substring(0, API_CB.Text.IndexOf("."))
        'Dim ModuleName As String = API_CB.Text.Substring(API_CB.Text.IndexOf(".") + 1, API_CB.Text.Length - API_CB.Text.IndexOf(".") - 1)
        'Dim x = From g In API_info_List Where g.APIName = APIName And g.Load_Module_name = Modulename Select g.Address
        'Get_API_Address = x.ElementAt(0)
        'Dim Get_API_Address_hex As String = Hex(Get_API_Address)
        'Dim xx As Boolean = DbgCmdExec("disasm " + Get_API_Address_hex)

    End Sub

    Private Sub GoTo_Dialog_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        'If Not Threading.Thread.CurrentThread.TrySetApartmentState(Threading.ApartmentState.STA) Then
        '    Throw New InvalidOperationException("This form is only usable from the UI thread")
        'End If
        'API_CB.DataSource = (From x In APIName_List Select x Distinct Order By x).ToArray
        'API_CB.AutoCompleteSource = Windows.Forms.AutoCompleteSource.ListItems
        'API_CB.AutoCompleteMode = Windows.Forms.AutoCompleteMode.SuggestAppend
    End Sub

    'Private Sub API_CB_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles API_CB.KeyUp
    '    If e.KeyCode = Windows.Forms.Keys.Enter Then Exit Sub
    '    Dim SearchTerm As String = API_CB.Text
    '    Dim ItemIndex = (From S As String In API_CB.Items.OfType(Of String)()
    '                    Where S.Contains(SearchTerm)
    '                    Select API_CB.FindStringExact(S)).DefaultIfEmpty(-1).FirstOrDefault
    '    API_CB.DroppedDown = True
    'End Sub

    Private Sub GoToDump_Bu_Click(sender As System.Object, e As System.EventArgs) Handles GoToDump_Bu.Click
        'Dim APIName As String = API_CB.Text.Substring(0, API_CB.Text.IndexOf("."))
        'Dim ModuleName As String = API_CB.Text.Substring(API_CB.Text.IndexOf(".") + 1, API_CB.Text.Length - API_CB.Text.IndexOf(".") - 1)
        'Dim x = From g In API_info_List Where g.APIName = APIName And g.Load_Module_name = ModuleName Select g.Address
        'Get_API_Address = x.ElementAt(0)
        'Dim Get_API_Address_hex As String = Hex(Get_API_Address)
        'Dim xx As Boolean = DbgCmdExec("dump " + Get_API_Address_hex)
        Dim APIName As String
        Dim Modulename As String
        Dim GetAPI As String = API_CB.Text
        Dim tt As String = GetAPI
        Dim LastDotIndex As Integer = GetAPI.LastIndexOf(".")
        Do Until tt = "."
            LastDotIndex = LastDotIndex - 1
            tt = GetAPI.Substring(LastDotIndex - 1, 1)
        Loop
        APIName = GetAPI.Substring(0, LastDotIndex - 1)
        Modulename = GetAPI.Substring(GetAPI.IndexOf(".") + 1, GetAPI.Length - GetAPI.IndexOf(".") - 1)
        Dim AddressOFAPI As Int64
        'For Symbols * and other ...search not return avalue so I do this way 
        If APIName.Contains("*") Then
            Dim FindAllHaveStarngeSymble = From j In API_info_List Where (j.APIName.Contains("*") And j.Load_Module_name = Modulename)
            For xx = 0 To FindAllHaveStarngeSymble.Count - 1
                Dim ll As String = FindAllHaveStarngeSymble.ElementAt(xx).APIName.ToString.ToLower
                If (ll.ToLower = APIName) Then
                    Dim j = FindAllHaveStarngeSymble.ElementAt(xx).Address
                    DbgCmdExec("dump " & Hex(j))
                    Exit For
                End If
            Next
        Else
            AddressOFAPI = (From j In API_info_List Where (j.APIName = APIName And j.Load_Module_name = Modulename) Select j.Address).First()
            DbgCmdExec("dump " & Hex(AddressOFAPI))
        End If
    End Sub

    Private Sub BULoadCB_Click(sender As System.Object, e As System.EventArgs) Handles BULoadCB.Click
        API_CB.DataSource = (From x In APIName_List Select x Distinct Order By x).ToArray
    End Sub
End Class