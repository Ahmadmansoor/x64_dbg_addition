﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("x64_dbg_VB")> 
<Assembly: AssemblyDescription("Dll Support for Strong x64_dbg Plugin")> 
<Assembly: AssemblyCompany("exetools")> 
<Assembly: AssemblyProduct("x64_dbg_VB")> 
<Assembly: AssemblyCopyright("Ahmadmansoor & mrexodiaCopyright ©  2014")> 
<Assembly: AssemblyTrademark("exetools")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("7e360599-a40b-40d7-b304-d609f97818d8")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.2")> 
<Assembly: AssemblyFileVersion("1.0.0.2")> 
