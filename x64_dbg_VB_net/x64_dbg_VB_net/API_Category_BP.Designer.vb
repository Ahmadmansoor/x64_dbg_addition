﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class API_Category_BP
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TreeView_API = New System.Windows.Forms.TreeView()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.AddMainTreeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripTextBoxAddMainTree = New System.Windows.Forms.ToolStripTextBox()
        Me.AddSubTreeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripTextBox_AddSubTree = New System.Windows.Forms.ToolStripTextBox()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RenameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripTextBox_rename = New System.Windows.Forms.ToolStripTextBox()
        Me.SaveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TB_Find3 = New System.Windows.Forms.TextBox()
        Me.TB_Find2 = New System.Windows.Forms.TextBox()
        Me.TB_Find1 = New System.Windows.Forms.TextBox()
        Me.BU_GO = New System.Windows.Forms.Button()
        Me.CB_Result = New System.Windows.Forms.ComboBox()
        Me.BU_Add = New System.Windows.Forms.Button()
        Me.TB_Find0 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BuLoadCB = New System.Windows.Forms.Button()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TreeView_API
        '
        Me.TreeView_API.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView_API.Location = New System.Drawing.Point(12, 49)
        Me.TreeView_API.Name = "TreeView_API"
        Me.TreeView_API.ShowNodeToolTips = True
        Me.TreeView_API.Size = New System.Drawing.Size(306, 319)
        Me.TreeView_API.TabIndex = 1
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddMainTreeToolStripMenuItem, Me.AddSubTreeToolStripMenuItem, Me.EditToolStripMenuItem, Me.RenameToolStripMenuItem, Me.SaveToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(153, 114)
        '
        'AddMainTreeToolStripMenuItem
        '
        Me.AddMainTreeToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripTextBoxAddMainTree})
        Me.AddMainTreeToolStripMenuItem.Name = "AddMainTreeToolStripMenuItem"
        Me.AddMainTreeToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.AddMainTreeToolStripMenuItem.Text = "Add Main Tree"
        '
        'ToolStripTextBoxAddMainTree
        '
        Me.ToolStripTextBoxAddMainTree.Name = "ToolStripTextBoxAddMainTree"
        Me.ToolStripTextBoxAddMainTree.Size = New System.Drawing.Size(100, 23)
        '
        'AddSubTreeToolStripMenuItem
        '
        Me.AddSubTreeToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripTextBox_AddSubTree})
        Me.AddSubTreeToolStripMenuItem.Name = "AddSubTreeToolStripMenuItem"
        Me.AddSubTreeToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.AddSubTreeToolStripMenuItem.Text = "Add Sub Tree"
        '
        'ToolStripTextBox_AddSubTree
        '
        Me.ToolStripTextBox_AddSubTree.Name = "ToolStripTextBox_AddSubTree"
        Me.ToolStripTextBox_AddSubTree.Size = New System.Drawing.Size(100, 23)
        Me.ToolStripTextBox_AddSubTree.ToolTipText = "Double Click To Get API From the CombBox"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.EditToolStripMenuItem.Text = "Delete"
        '
        'RenameToolStripMenuItem
        '
        Me.RenameToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripTextBox_rename})
        Me.RenameToolStripMenuItem.Name = "RenameToolStripMenuItem"
        Me.RenameToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.RenameToolStripMenuItem.Text = "Rename"
        '
        'ToolStripTextBox_rename
        '
        Me.ToolStripTextBox_rename.Name = "ToolStripTextBox_rename"
        Me.ToolStripTextBox_rename.Size = New System.Drawing.Size(100, 23)
        '
        'SaveToolStripMenuItem
        '
        Me.SaveToolStripMenuItem.Name = "SaveToolStripMenuItem"
        Me.SaveToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.SaveToolStripMenuItem.Text = "Save"
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownWidth = 380
        Me.ComboBox1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(12, 374)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(250, 22)
        Me.ComboBox1.TabIndex = 2
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TB_Find3)
        Me.GroupBox1.Controls.Add(Me.TB_Find2)
        Me.GroupBox1.Controls.Add(Me.TB_Find1)
        Me.GroupBox1.Controls.Add(Me.BU_GO)
        Me.GroupBox1.Controls.Add(Me.CB_Result)
        Me.GroupBox1.Controls.Add(Me.BU_Add)
        Me.GroupBox1.Controls.Add(Me.TB_Find0)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 399)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(306, 82)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        '
        'TB_Find3
        '
        Me.TB_Find3.Location = New System.Drawing.Point(188, 20)
        Me.TB_Find3.Name = "TB_Find3"
        Me.TB_Find3.Size = New System.Drawing.Size(54, 20)
        Me.TB_Find3.TabIndex = 3
        '
        'TB_Find2
        '
        Me.TB_Find2.Location = New System.Drawing.Point(128, 21)
        Me.TB_Find2.Name = "TB_Find2"
        Me.TB_Find2.Size = New System.Drawing.Size(54, 20)
        Me.TB_Find2.TabIndex = 2
        '
        'TB_Find1
        '
        Me.TB_Find1.Location = New System.Drawing.Point(68, 21)
        Me.TB_Find1.Name = "TB_Find1"
        Me.TB_Find1.Size = New System.Drawing.Size(54, 20)
        Me.TB_Find1.TabIndex = 1
        '
        'BU_GO
        '
        Me.BU_GO.Location = New System.Drawing.Point(257, 20)
        Me.BU_GO.Name = "BU_GO"
        Me.BU_GO.Size = New System.Drawing.Size(41, 20)
        Me.BU_GO.TabIndex = 4
        Me.BU_GO.Text = "GO"
        Me.BU_GO.UseVisualStyleBackColor = True
        '
        'CB_Result
        '
        Me.CB_Result.FormattingEnabled = True
        Me.CB_Result.Location = New System.Drawing.Point(8, 46)
        Me.CB_Result.Name = "CB_Result"
        Me.CB_Result.Size = New System.Drawing.Size(242, 21)
        Me.CB_Result.TabIndex = 5
        '
        'BU_Add
        '
        Me.BU_Add.Location = New System.Drawing.Point(257, 45)
        Me.BU_Add.Name = "BU_Add"
        Me.BU_Add.Size = New System.Drawing.Size(41, 20)
        Me.BU_Add.TabIndex = 6
        Me.BU_Add.Text = "Add"
        Me.BU_Add.UseVisualStyleBackColor = True
        '
        'TB_Find0
        '
        Me.TB_Find0.Location = New System.Drawing.Point(8, 20)
        Me.TB_Find0.Name = "TB_Find0"
        Me.TB_Find0.Size = New System.Drawing.Size(54, 20)
        Me.TB_Find0.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(301, 14)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "F2= Soft Breck Point ,F3=Hardware Breck Point"
        '
        'BuLoadCB
        '
        Me.BuLoadCB.Location = New System.Drawing.Point(269, 374)
        Me.BuLoadCB.Name = "BuLoadCB"
        Me.BuLoadCB.Size = New System.Drawing.Size(49, 23)
        Me.BuLoadCB.TabIndex = 3
        Me.BuLoadCB.Text = "Load"
        Me.BuLoadCB.UseVisualStyleBackColor = True
        '
        'API_Category_BP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(329, 492)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BuLoadCB)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.TreeView_API)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "API_Category_BP"
        Me.Text = "API_Category_BP"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TreeView_API As System.Windows.Forms.TreeView
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RenameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddSubTreeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripTextBox_AddSubTree As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripTextBox_rename As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents AddMainTreeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripTextBoxAddMainTree As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents BuLoadCB As System.Windows.Forms.Button
    Friend WithEvents TB_Find0 As System.Windows.Forms.TextBox
    Friend WithEvents BU_Add As System.Windows.Forms.Button
    Friend WithEvents BU_GO As System.Windows.Forms.Button
    Friend WithEvents CB_Result As System.Windows.Forms.ComboBox
    Friend WithEvents TB_Find3 As System.Windows.Forms.TextBox
    Friend WithEvents TB_Find2 As System.Windows.Forms.TextBox
    Friend WithEvents TB_Find1 As System.Windows.Forms.TextBox
End Class
